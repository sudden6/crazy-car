function sharp_sensor()
more off;
close all;
# 
# vADCval = [3770, 3180,2495,1977,1667,1400,1230,1100,995,914,840,782,727,672,641];
#vDistance = [140,240,340,440,540,640,740,840,940,1040,1140,1240,1340,1440,1540];

#left sensor
#vADCval = [4095, 3475, 1890, 1329, 1052, 879, 768, 689, 550, 471, 416];
#vDistance = [55, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];

#right sensor
vADCval = [4095, 4009, 2060, 1429, 1152, 914, 776, 590, 478, 359, 266];
vDistance = [55, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];

vADCfullRange = 150:1:4095;

# minimize the RMS difference for k
frms = @(x) getQuadrMw(x, vDistance, vADCval)(1);
sk = fminsearch(frms, 0);
sk = round(sk)
[sRMS, sm, sb] = getQuadrMw(sk, vDistance, vADCval)
sM = round(1/sm)
sB = round(sb/sm)

vDiff = SHARP(vADCfullRange, sm, sb, sk) - SHARPuC(vADCfullRange, sM, sB, sk);


# plot the RMS over different k
vk = 0:1:1000;
vrms = arrayfun(frms,vk);
figure
plot(vk, vrms);

# plot the measured values vs the interpolation
figure
plot(vADCval, vDistance, 'x', vADCfullRange, SHARP(vADCfullRange, sm, sb, sk))

# plot the difference between the float and int implementation
figure
plot(vADCfullRange, vDiff);
end


# calculate the RMS difference between the calculation and the measurments
# for a given k
function [sRMS, sm, sb] = getQuadrMw(sk, vDistance, vADCval)
vV = 1./(vDistance + sk);
# calculate coefficients
p = polyfit(vADCval, vV, 1);
vInterp = SHARP(vADCval, p(1), p(2), sk);
sRMS = norm(vDistance - vInterp);
sm = p(1);
sb = p(2);
end

function vDist = SHARP(vADCval, sm, sb, sk)
 sM = 1/sm;
 sB = sb/sm;
 vDist =sM./(vADCval + sB) - sk; 
end

function vDist = SHARPuC(vADCval, sM, sB, sK)
 vDist = idivide(sM,vADCval + sB) - sK; 
end

