/*
 * ringbuffer.h
 *
 *  Created on: 25.01.2017
 *      Author: christoph
 */

#ifndef UTIL_RINGBUFFER_H_
#define UTIL_RINGBUFFER_H_

#include "inttypes.h"

struct UT_RingBufferU16
{
	uint16_t *buf_first;	// points to the first usable address in memory
	uint16_t *buf_last;		// points to the last usable address in memory
	uint16_t *head;			// points to the first free element in the ring buffer
	uint16_t *tail;			// points to the first used element in the ring buffer
};

void UT_RingBufferU16_Init(struct UT_RingBufferU16 *ring, uint16_t *memory, uint16_t size);
uint16_t *UT_RingBufferU16_GetLastHead(struct UT_RingBufferU16 *ring, uint16_t index);
void UT_RingBufferU16_Insert(struct UT_RingBufferU16 *ring, uint16_t data);
void UT_RingBufferU16_InsertOvw(struct UT_RingBufferU16 *ring, uint16_t data);

#endif /* UTIL_RINGBUFFER_H_ */
