/*
 * lowpass.c
 *
 *  Created on: 16.01.2017
 *      Author: christoph
 */

#include <inttypes.h>

// BANDWITH = 0.1197 * SAMPLE_FREQ
#define FILTER_SHIFT 1
// only works for 15Bit input
uint16_t lowpass(volatile uint16_t* tmp, uint16_t input)
{
	// Update filter with current sample.
	(*tmp) = (*tmp) - ((*tmp) >> FILTER_SHIFT) + input;
	// Scale output for unity gain.
	return (*tmp) >> FILTER_SHIFT;
}



