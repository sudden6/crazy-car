/*
 * controller.h
 *
 *  Created on: 09.01.2017
 *      Author: hansi
 */

#ifndef UTIL_CONTROLLER_H_
#define UTIL_CONTROLLER_H_

#include <inttypes.h>

#define PID_UPDATE_FREQ	100

/*
typedef struct
{
	int16_t e;
	int16_t kp;
	uint8_t kp_denom;
	int16_t ki;
	uint8_t ki_denom;
	int16_t kd;
	uint8_t kd_denom;
	int16_t ta;
	int16_t satUp;
	int16_t satLow;

	int16_t esum;
	int16_t ealt;
} PID;

typedef struct
{
	int16_t sollwert;
	int16_t istwert;
	int32_t y;

	PID parameter;
}PIDContr;
//*/
typedef struct
{
	float e;
	float kp;
	float ki;
	float kd;
	float ta;
	float satUp;
	float satLow;

	float esum;
	float ealt;
} PID;

typedef struct
{
	int enabled;
	float sollwert;
	float istwert;
	float y;

	PID parameter;
}PIDContr;


void ControlPID(PIDContr *c);
void HandleControllers(PIDContr *c, int16_t num);
void ResetPID(PIDContr *c);

#endif /* UTIL_CONTROLLER_H_ */
