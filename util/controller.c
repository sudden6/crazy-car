/*
 * controller.c
 *
 *  Created on: 09.01.2017
 *      Author: hansi
 */

#include <inttypes.h>
#include "controller.h"

void ControlPID(PIDContr *c)
{
	c->parameter.e = c->sollwert - c->istwert;

	//*
	if((c->y > c->parameter.satLow) && (c->y < c->parameter.satUp))
	{
		c->parameter.esum = c->parameter.esum + c->parameter.e;							//anti-windup
	}
	//*/

	c->y = (c->parameter.kp * c->parameter.e);											//p-part
	c->y += (c->parameter.ki * c->parameter.ta * c->parameter.esum);						//i-part
	c->y += (c->parameter.kd * (c->parameter.e - c->parameter.ealt) / c->parameter.ta);	//d-part
	c->parameter.ealt = c->parameter.e;

	//*
	if(c->y < c->parameter.satLow)
	{
		c->y = c->parameter.satLow;
	}
	else if(c->y > c->parameter.satUp)
	{
		c->y = c->parameter.satUp;
	}
	//*/
}

void HandleControllers(PIDContr *c, int16_t num)
{
	int i;
	for(i = 0; i < num; i++)
	{
		ControlPID(c);
		c++;
	}
}

void ResetPID(PIDContr *c)
{
	c->parameter.e = 0;
	c->parameter.ealt = 0;
	c->parameter.esum = 0;
}
