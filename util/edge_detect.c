/*
 * edge_detect.c
 *
 *  Created on: 25.01.2017
 *      Author: christoph
 */

#include <inttypes.h>
#include "edge_detect.h"
#include "ringbuffer.h"

// detects if an edge exists in the buffer
// buf       : ring buffer where the data is stored
// avg_cnt   : number of elements that should be averaged, the buffer has to be twice this size
// threshold : threshold that has to be reached to detect an edge
// returns   : -1 if a negative edge, +1 if a positive edge and 0 if no edge has been detected before the last avg_cnt values
int UT_Edge_Detect(struct UT_RingBufferU16 *buf, uint16_t avg_cnt, uint16_t threshold)
{
	int i;
	uint32_t sum_before = 0;
	uint32_t sum_after = 0;
	for(i = 0; i < (avg_cnt*2); i++)
	{
		if(i < avg_cnt)
		{
			sum_after += *UT_RingBufferU16_GetLastHead(buf, i);
		}
		else
		{
			sum_before += *UT_RingBufferU16_GetLastHead(buf, i);
		}
	}

	int32_t diff = sum_after - sum_before;
	uint32_t new_thres = threshold * avg_cnt;
	if(abs(diff) < new_thres)
	{
		return 0;
	}
	else if(diff > 0)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}


