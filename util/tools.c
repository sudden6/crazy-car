/*
 * tools.c
 *
 *  Created on: 08.11.2016
 *      Author: hansi
 */

#include <inttypes.h>
#include <string.h>
#include "../HAL/hal_ucs.h"
#include "tools.h"

uint16_t map_16(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max)
{
	uint32_t tmp =  (x - in_min) * (uint32_t)(out_max - out_min) / (in_max - in_min) + out_min;
	return (uint16_t)tmp;
}

uint32_t map_32(uint32_t x, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max)
{
	uint64_t tmp =  (x - in_min) * (uint64_t)(out_max - out_min) / (in_max - in_min) + out_min;
	return (uint32_t)tmp;
}

void _delay_s(uint16_t time)
{
	int i;
	for(i = 0; i < time; i++)
	{
		__delay_cycles(MCLK_FREQU);
	}
}

void _delay_ms(uint16_t time)
{
	int i;
	for(i = 0; i < time; i++)
	{
		__delay_cycles(MCLK_FREQU / 1000);
	}
}

void ReverseString(char s[])
{
    int16_t i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void IntToString(int32_t n, char s[])
{
    int16_t i;
	int32_t sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */

    if (sign < 0)
    {
        s[i++] = '-';
    }

    s[i] = '\0';
    ReverseString(s);
}

void UIntToString(uint32_t n, char s[])
{
    int16_t i;

    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */

    s[i] = '\0';
    ReverseString(s);
}

// Writes an uint16_t to a string with a fixed width
// n: the number which gets displayed
// width: how many digits the string will have
// s: the string buffer to write to
void UInt16ToStringWidth(uint16_t n, uint16_t width, char s[])
{
	char tmp[6];	// uint16_t can have max 5 digits
	memset(tmp, ' ', 6);
    int i = 0;
    do {       /* generate digits in reverse order */
        tmp[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */

    int j = 0;
    for(;j < width; j++)
    {
    	s[j] = tmp[width - j - 1];
    }
    s[j] = '\0';
}

void FloatToString(char *str, float f, uint8_t size)
{
	char pos;  // position in string

    char len;  // length of decimal part of result

    char tmp[7] = {0};
    char *curr = tmp;  // temp holder for next digit

    int value;  // decimal digit(s) to convert

    pos = 0;  // initialize pos, just to be sure

    value = (int)f;  // truncate the floating point number
    IntToString(value,str);  // this is kinda dangerous depending on the length of str
    // now str array has the digits before the decimal

    if (f < 0 )  // handle negative numbers
    {
        f *= -1;
        value *= -1;
    }

    len = strlen(str);  // find out how big the integer part was
    pos = len;  // position the pointer to the end of the integer part
    str[pos++] = '.';  // add decimal point to string

    while(pos < (size + len + 1) )  // process remaining digits
    {
        f = f - (float)value;  // hack off the whole part of the number
        f *= 10;  // move next digit over
        value = (int16_t)f;  // get next digit
        IntToString(value, curr); // convert digit to string
        str[pos++] = *curr; // add digit to result string and increment pointer
    }
 }

void UIntToHex(uint32_t number, char *str)
{
	static const char digits[] = "0123456789ABCDEF";

#define mask	0x0000000F

	uint32_t i = 0;
	char *curr_pos = &str[2];

	str[0] = '0';
	str[1] = 'x';

	do
	{
		i = (number & mask);
		number >>= 4;
		*curr_pos = digits[i];
		curr_pos++;
	}
	while(number > 0);

	*curr_pos = '\0';

	ReverseString(&str[2]);
}

