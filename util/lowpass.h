/*
 * lowpass.h
 *
 *  Created on: 16.01.2017
 *      Author: christoph
 */

#ifndef UTIL_LOWPASS_H_
#define UTIL_LOWPASS_H_

#include <inttypes.h>

uint16_t lowpass(volatile uint16_t* tmp, uint16_t input);

#endif /* UTIL_LOWPASS_H_ */
