/*
 * edge_detect.h
 *
 *  Created on: 25.01.2017
 *      Author: christoph
 */

#include <inttypes.h>
#include "ringbuffer.h"

#ifndef UTIL_EDGE_DETECT_H_
#define UTIL_EDGE_DETECT_H_

int UT_Edge_Detect(struct UT_RingBufferU16 *buf, uint16_t avg_cnt, uint16_t threshold);

#endif /* UTIL_EDGE_DETECT_H_ */
