/*
 * ringbuffer.c
 *
 *  Created on: 25.01.2017
 *      Author: christoph
 */

#include <inttypes.h>
#include "ringbuffer.h"

// initializes a ring buffer in the given memory (usually a static array)
// ring  : RingBuffer struct which should be initialized
// memory: pointer to the start address of the memory (e.g. array[0])
// size  : number of elements in the ring buffer, must be >0
void UT_RingBufferU16_Init(struct UT_RingBufferU16 *ring, uint16_t *memory, uint16_t size)
{
	ring->buf_first = memory;
	ring->buf_last = memory + size - 1;
	ring->head = ring->buf_first;
	ring->tail = ring->buf_last;
}

// Get a pointer to the n-th last element written to the buffer
// ring   : RingBuffer struct to operate on
// index  : if 0, a pointer to the last element written, if 1, to the second last element,...
// returns: a pointer to the n-th last element
uint16_t *UT_RingBufferU16_GetLastHead(struct UT_RingBufferU16 *ring, uint16_t index)
{
	// check if reading below the buffer
	if((ring->head - 1 - index) < ring->buf_first)
	{
		index -= ring->head - ring->buf_first;
		return ring->buf_last - index;
	}
	else
	{
		return ring->head - 1 - index;
	}
}

// Inserts data into the ring buffer
// ring: RingBuffer struct to operate on
// data: element to insert into the buffer
void UT_RingBufferU16_Insert(struct UT_RingBufferU16 *ring, uint16_t data)
{
	// check for buffer overflow
	if(ring->head == ring->tail)
	{
		return;
	}

	// write data
	*(ring->head) = data;

	// handle head pointer wrap around
	if(ring->head == ring->buf_last)
	{
		ring->head = ring->buf_first;
	}
	else
	{
		ring->head++;
	}
}

// Inserts data into the ring buffer, overwriting old data
// ring: RingBuffer struct to operate on
// data: element to insert into the buffer
void UT_RingBufferU16_InsertOvw(struct UT_RingBufferU16 *ring, uint16_t data)
{
	// write data
	*(ring->head) = data;

	// handle head pointer wrap around
	if(ring->head == ring->buf_last)
	{
		ring->head = ring->buf_first;
	}
	else
	{
		ring->head++;
	}
}


