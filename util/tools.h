/*
 * tools.h
 *
 *  Created on: 08.11.2016
 *      Author: hansi
 */

#ifndef UTIL_TOOLS_H_
#define UTIL_TOOLS_H_

#include <inttypes.h>

#define PI				3.141592	//PI
#define PI_INT			3141

uint16_t map_16(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max);
uint32_t map_32(uint32_t x, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max);

void _delay_s(uint16_t time);
void _delay_ms(uint16_t time);

void IntToString(int32_t value, char* result);
void UIntToString(uint32_t n, char s[]);
void UInt16ToStringWidth(uint16_t n, uint16_t width, char s[]);
void FloatToString(char *str, float f, uint8_t size);
void UIntToHex(uint32_t number, char *str);

void ReverseString(char s[]);

#endif /* UTIL_TOOLS_H_ */
