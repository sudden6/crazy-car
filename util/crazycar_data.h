/*
 * crazycar_data.h
 *
 *  Created on: 15.11.2016
 *      Author: hansi
 */

#ifndef UTIL_CRAZYCAR_DATA_H_
#define UTIL_CRAZYCAR_DATA_H_

#define NUMBER_MAGNETS	22			//number of magnets which are in the motor
#define DIAMETER_WHEEL	36			//diameter of the wheels in mm (measured)
#define CIRCUMFERENCE_WHEEL 112		//circumference of wheels in mm (measured)

#define VBAT_R1			33UL		//value of the resistance R1 in Kiloohm
#define VBAT_R2			120UL		//value of the resistance R2 in Kiloohm

#endif /* UTIL_CRAZYCAR_DATA_H_ */
