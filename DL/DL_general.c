/*
 * driver_general.c
 *
 *  Created on: 08.11.2016
 *      Author: hansi
 */

#include "DL_aktorik.h"
#include "DL_display.h"
#include "DL_WLAN.h"

void DL_Init()
{
	DL_Aktorik_InitThrottle(THROTTLE_MODE_PWM);
	DL_Display_Init();
	//DL_WLAN_Init();
}
