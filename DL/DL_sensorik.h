/*
 * DL_distance.h
 *
 *  Created on: 12.12.2016
 *      Author: christoph
 */

#ifndef DL_DL_SENSORIK_H_
#define DL_DL_SENSORIK_H_

uint16_t DL_Sensorik_GetDistanceFront();
uint16_t DL_Sensorik_GetDistanceLeft();
uint16_t DL_Sensorik_GetDistanceRight();
uint16_t DL_Sensorik_GetVBat();
int32_t DL_Sensorik_GetSpeed();
int32_t DL_Sensorik_GetCoveredDistance();

#endif /* DL_DL_SENSORIK_H_ */
