/*
 * DL_display.h
 *
 *  Created on: 22.11.2016
 *      Author: christoph
 */

#ifndef DL_DL_DISPLAY_H_
#define DL_DL_DISPLAY_H_

void DL_Display_Init();
void DL_Display_Clear();

void DL_Display_SetPosition(uint8_t page, uint8_t col);

void DL_Display_WriteString (char *text, uint8_t row, uint8_t col);

void DL_Display_WriteInt(int32_t number, uint8_t row, uint8_t col);
void DL_Display_WriteUInt(uint32_t number, uint8_t row, uint8_t col);
void DL_Display_WriteUInt16(uint16_t number, uint16_t width, uint8_t row, uint8_t col);
void DL_Display_WriteHex(uint32_t number, uint8_t row, uint8_t col);

void DL_Display_WriteFloat(float number, uint8_t digits, uint8_t row, uint8_t col);

#endif /* DL_DL_DISPLAY_H_ */
