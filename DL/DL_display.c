/*
 * DL_display.c
 *
 *  Created on: 22.11.2016
 *      Author: christoph
 */

#include <inttypes.h>
#include "DL_fonttable.h"
#include "../HAL/hal_gpio.h"
#include "../HAL/hal_usciB1.h"
#include "../util/tools.h"

#define NUM_PAGES 8									// number of pages of our display
#define NUM_COLS 128								// number of columns of our display = horizontal pixels
#define CMD_PAGE_ADDR(x) (0b10110000 | (0x0F & x))	// cmd to change the page of the display RAM
#define CMD_COL_HADDR(x) (0b00010000 | (0x0F & x))	// set the high bits of the column address
#define CMD_COL_LADDR(x) (0b00000000 | (0x0F & x))	// set the low bits of the column address

// default init commands from script
enum DISPLAY_INIT_CMD {
	DISPLAY_RESET =		0xE2,
	DISPLAY_BIAS =		0xA3,
	ADC_SEL_NORMAL =	0xA0,
	COMMON_REVERSE =	0xC8,
	RES_RATIO =			0x24,
	ELEC_VOL_MODE =		0x81,
	ELEC_VOL_VALUE =	0x0F,
	POWER_CONT =		0x2F,
	DISPLAY_ON =		0xAF
};

// Writes commands to the display
// data: command sequence to be sent
// len : number of bytes in the command sequence
static void DL_Display_WriteCommand(uint8_t *data, uint8_t len)
{
	LOW(LCD_SPI_CS);						// set the chip select low
	__delay_cycles(4);
	LOW(LCD_DATACMD);						// set command pin low -> command mode

	__delay_cycles(4);
	HAL_USCIB1_SendBytes(data, len);		// send the command
	while(HAL_USCIB1_Busy());				// wait until command sent

	HIGH(LCD_DATACMD);						// set command pin high -> data mode
	__delay_cycles(4);
	HIGH(LCD_SPI_CS);						// set the chip select high
	__delay_cycles(4);
}

// Writes data to the display
// data: data sequence to be sent
// len : number of bytes in the data sequence
static void DL_Display_WriteData(uint8_t *data, uint8_t len)
{
	LOW(LCD_SPI_CS);

	__delay_cycles(4);
	HAL_USCIB1_SendBytes(data, len);
	while(HAL_USCIB1_Busy());				// wait until command sent

	__delay_cycles(4);
	HIGH(LCD_SPI_CS);						// set the chip select high
	__delay_cycles(4);

}

// sets the display RAM to all zeros
void DL_Display_Clear()
{

	uint8_t page, col;
	for(page = 0; page < NUM_PAGES; page++)
	{
		uint8_t cmd[] = {CMD_PAGE_ADDR(page), CMD_COL_HADDR(0), CMD_COL_LADDR(0)};
		DL_Display_WriteCommand(cmd, 3);

		LOW(LCD_SPI_CS);
		__delay_cycles(4);
		for(col = 0; col < NUM_COLS; col++)
		{
			HAL_USCIB1_SendByteWait(0x00);
		}
		__delay_cycles(4);
		HIGH(LCD_SPI_CS);						// set the chip select high
		__delay_cycles(4);

	}
}

// initialize the display
void DL_Display_Init()
{
	// reset the display, see script
	LOW(LCD_RESET);
	_delay_ms(1);
	HIGH(LCD_RESET);
	_delay_ms(1);

	// default settings from the script
	uint8_t init_commands[] = {DISPLAY_RESET, DISPLAY_BIAS, ADC_SEL_NORMAL,
								COMMON_REVERSE, RES_RATIO, ELEC_VOL_MODE,
								ELEC_VOL_VALUE, POWER_CONT, DISPLAY_ON };
	DL_Display_WriteCommand(init_commands, 9);
	DL_Display_Clear();
	LCD_BL_ON;
}

// Writes a string to the display
// *text : pointer to string which gets displayed
// row: the page number (row) in where the text should be displayed
// col: the column where the text starts
void DL_Display_WriteString (char *text, uint8_t row, uint8_t col)
{
	uint8_t cmd[] = {CMD_PAGE_ADDR(row), CMD_COL_HADDR(col >> 4), CMD_COL_LADDR(col)};		//create command to set the right position
	DL_Display_WriteCommand(cmd, 3);								//write command to display

	uint8_t available_letters = (NUM_COLS - col) / FONT_WIDTH;		//how many characters can be added in the current row

	while(*text != '\0')											//while the string isn't empty
	{
		if(available_letters > 0)
		{
			//the row has free space for at least one more letter

			DL_Display_WriteData((uint8_t *)font_table[*text],6);	//write letter to display
			available_letters--;									//decrement the available letters
			text++;													//increment the pointer of the string
		}
		else
		{
			//no space for another letter --> next row(page)

			row++;
			if(row >= NUM_PAGES)
			{
				//reached already the last row, function quits
				return;
			}

			//change to the next row (page)

			//create command
			cmd[0] = CMD_PAGE_ADDR(row);
			cmd[1] = CMD_COL_HADDR(0);
			cmd[2] = CMD_COL_LADDR(0);

			DL_Display_WriteCommand(cmd, 3);			//write command
			available_letters = NUM_COLS / FONT_WIDTH;	//calculate free letters
		}
	}
}

// Writes an unsigned integer to the display
// number: the number which gets displayed
// row: the page number (row) in where the text should be displayed
// col: the column where the text starts
void DL_Display_WriteUInt(uint32_t number, uint8_t row, uint8_t col)
{
	char text[11] = {0}; 					//array to store the string (uint32_t max. 10 chars + end of string)
	UIntToString(number, text); 			//convert number to string
	DL_Display_WriteString(text, row, col);	//write converted number as string to display
}

// Writes an integer to the display
// number: the number which gets displayed
// row: the page number (row) in where the text should be displayed
// col: the column where the text starts
void DL_Display_WriteInt(int32_t number, uint8_t row, uint8_t col)
{
	char text[12] = {0};					//array to store the string (int32_t max. 11 chars + end of string)
	IntToString(number, text);				//convert number to string
	DL_Display_WriteString(text, row, col);	//write converted number as string to display
}

// Writes a float to the display
// number: the number which gets displayed
// digits: how many digits should be displayed after the comma
// row: the page number (row) in where the text should be displayed
// col: the column where the text starts
void DL_Display_WriteFloat(float number, uint8_t digits, uint8_t row, uint8_t col)
{
	char text[22] = {0};					//array to store the string (maximum 21 chars in one row + end of string)
	FloatToString(text, number, digits);	//convert number to string
	DL_Display_WriteString(text, row, col);	//write converted number as string to display
}

// Displays and unsigned integer in hex
// number: the number which gets displayed
// row: the page number (row) in where the text should be displayed
// col: the column where the text starts
void DL_Display_WriteHex(uint32_t number, uint8_t row, uint8_t col)
{
	char text[11] = { 0 };					//array to store the string(uint32_t max. 10 chars + end of string)
	UIntToHex(number, text);				//convert number to string
	DL_Display_WriteString(text, row, col);	//write converted number as string to display
}

// Sets the cursor to a certain position
// row: the page number (row) in where the text should be displayed
// col: the column where the text starts
void DL_Display_SetPosition(uint8_t page, uint8_t col)
{
	uint8_t cmd[] = {CMD_PAGE_ADDR(page), CMD_COL_HADDR(col >> 4), CMD_COL_LADDR(col)};	//create command to set the new position
	DL_Display_WriteCommand(cmd, 3);													//write command
}

// Writes an unsigned integer to the display
// number: the number which gets displayed
// width: how many chars the text will be wide
// row: the page number (row) in where the text should be displayed
// col: the column where the text starts
void DL_Display_WriteUInt16(uint16_t number, uint16_t width, uint8_t row, uint8_t col)
{
	char text[6] = {0}; 						//array to store the string (uint32_t max. 10 chars + end of string)
	UInt16ToStringWidth(number, width, text); 	//convert number to string
	DL_Display_WriteString(text, row, col);		//write converted number as string to display
}
