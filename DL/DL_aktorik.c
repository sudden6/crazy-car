/*
 * driver_aktorik.c
 *
 *  Created on: 08.11.2016
 *      Author: hansi
 */

#include <inttypes.h>
#include "DL_aktorik.h"
#include "../util/tools.h"
#include "../HAL/hal_timerA1.h"
#include "../HAL/hal_ucs.h"
#include "../HAL/hal_gpio.h"


//send a certain number of pwm-pulses
//pwm: the dutycycle for the pwm
//cnt: number of the pulses to send
//channel: which pwm-channel is used
void SendPulses(uint16_t pwm, uint16_t cnt, uint16_t channel)
{
#define PULSE_CYCLES 350000 //cycles for 1 pulse by 60Hz, is not an exact number, has been tried out until it worked

	int i;
	HAL_TimerA1_SetPulseLength(pwm, channel);

	for(i = 0; i < cnt; i++)
	{
		__delay_cycles(PULSE_CYCLES);
	}

}

//sets the steering
//val: -100 max. left to +100 max. right
void DL_Aktorik_SetSteering(int16_t val) //calculates -100 to +100 into microseconds for the PWM
{
	uint16_t us = 0;
	if(val < MAX_LEFT)
	{
		us = LEFT_US;
	}
	else if(val > MAX_RIGHT)
	{
		us = RIGHT_US;
	}
	else
	{
		val += -MAX_LEFT; //to avoid negative numbers for the map-function
		us = us = map_16(val,0,(-MAX_LEFT)+MAX_RIGHT,LEFT_US,RIGHT_US);
	}

	HAL_TimerA1_SetPulseLength(us, CHANNEL_STEERING);
}

//sets the speed of the car
//speed: -100 max. left 0 breaks, +100 max. right
void DL_Aktorik_SetThrottle(int16_t speed) //calculates -100 to +100 into microseconds for the PWM
{
	speed += 100;
	uint16_t us = 0;
	if(speed < MAX_REVERSE)
	{
		us = FORWARD_US_MAX;
	}
	else if(speed > MAX_FORWARD)
	{
		us = FORWARD_US_MAX;
	}
	else if(speed < 100)
	{
		//map from REVERSE_US_MAX to REVERSE_US_MIN
		us = map_16(speed, MAX_REVERSE, (MAX_FORWARD + MAX_REVERSE)/2, REVERSE_US_MAX, REVERSE_US_MIN);
	}
	else if(speed > 100)
	{
		//map from FORWARD_US_MIN to FORWARD_US_MAX
		us = map_16(speed, (MAX_FORWARD + MAX_REVERSE) / 2, MAX_FORWARD, FORWARD_US_MIN, FORWARD_US_MAX);
	}
	else
	{
		//car breaks
		us = BREAK_MAX;
	}

	HAL_TimerA1_SetPulseLength(us, CHANNEL_THROTTLE);
}

//initializes the motor-controller
//mode: UART or PWM
void DL_Aktorik_InitThrottle(uint8_t mode)
{
	if(mode == THROTTLE_MODE_PWM)
	{
		//the numbers of cycles are from the datasheet  CrazyCar_HPI-ESC_Modified_Manual
		SendPulses(REVERSE_US_MAX, 131, CHANNEL_THROTTLE);
		SendPulses(REVERSE_US_MIN, 128, CHANNEL_THROTTLE);
		SendPulses(FORWARD_US_MIN, 128, CHANNEL_THROTTLE);
		SendPulses(FORWARD_US_MAX, 128, CHANNEL_THROTTLE);
		SendPulses(BREAK_MAX, 1, CHANNEL_THROTTLE);
	}
	else if (mode == THROTTLE_MODE_UART)
	{

	}
}


