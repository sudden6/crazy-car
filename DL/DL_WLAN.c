/*
 * DL_WLAN.c
 *
 *  Created on: 23.01.2017
 *      Author: hansi
 */

#include <inttypes.h>
#include "../util/tools.h"
#include "../HAL/hal_usciA1.h"

#define VARIABLE_COUNT 10 // the max value of variables per type that can be printed

struct UInt16_Log
{
	uint16_t *variables[VARIABLE_COUNT];
	uint16_t count;
}UInt16;

struct UInt32_Log
{
	uint32_t *variables[VARIABLE_COUNT];
	uint16_t count;
}UInt32;

struct Int16_Log
{
	int16_t *variables[VARIABLE_COUNT];
	uint16_t count;
}Int16;

struct Int32_Log
{
	int32_t *variables[VARIABLE_COUNT];
	uint16_t count;
}Int32;

struct Float_Lob
{
	float *variables[VARIABLE_COUNT];
	uint16_t count;
}Float;

void DL_WLAN_Init()
{
	HAL_USCIA1_WriteString("AT+CWMOD=3\r\n"); //start hotspot
	_delay_s(1);
	HAL_USCIA1_WriteString("AT+CWSAP=\"Team++\",\"abcdabcd\",1,3\r\n"); //wlan name, password, channel 1, wlan mode 3
	_delay_s(5);
	HAL_USCIA1_WriteString("AT+CIPMODE=1\r\n");	//UART-Wifi-Bridge mode
	_delay_s(1);
	HAL_USCIA1_WriteString("AT+CIPMUX=0\r\n"); //Single Connection
	_delay_s(1);
	HAL_USCIA1_WriteString("AT+CIPSTART=\"TCP\",\"192.168.4.2\",4444\r\n"); //UDP-Connection to 192.168.4.2 on Port 4444
	_delay_s(5);
	HAL_USCIA1_WriteString("AT+CIPSEND\r\n"); //Starts Transmission
	_delay_s(1);

}

void DL_WLAN_RegisterUInt16(uint16_t *ptr)
{
	UInt16.variables[UInt16.count] = ptr;
	UInt16.count++;
}

void DL_WLAN_RegisterUInt32(uint32_t *ptr)
{
	UInt32.variables[UInt32.count] = ptr;
	UInt32.count++;
}

void DL_WLAN_RegisterInt16(int16_t *ptr)
{
	Int16.variables[Int16.count] = ptr;
	Int16.count++;
}

void DL_WLAN_RegisterInt32(int32_t *ptr)
{
	Int32.variables[Int32.count] = ptr;
	Int32.count++;
}

void DL_WLAN_RegisterFloat(float *ptr)
{
	Float.variables[Float.count] = ptr;
	Float.count++;
}

void DL_WLAN_SendInt16(int16_t **data, uint16_t length)
{
	int i;

	char number[7] = {0};
	for(i = 0; i < length; i++)
	{
		IntToString(*(data[i]), number);
		HAL_USCIA1_WriteString(number);
		HAL_USCIA1_WriteString(";");
	}
}

void DL_WLAN_SendInt32(int32_t **data, uint16_t length)
{
	int i;

	char number[13] = {0};
	for(i = 0; i < length; i++)
	{
		IntToString(*(data[i]), number);
		HAL_USCIA1_WriteString(number);
		HAL_USCIA1_WriteString(";");
	}
}

void DL_WLAN_SendUInt16(uint16_t **data, uint16_t length)
{
	int i;

	char number[6] = {0};
	for(i = 0; i < length; i++)
	{
		UIntToString(*(data[i]), number);
		HAL_USCIA1_WriteString(number);
		HAL_USCIA1_WriteString(";");
	}
}

void DL_WLAN_SendUInt32(uint32_t **data, uint16_t length)
{
	int i;

	char number[12] = {0};
	for(i = 0; i < length; i++)
	{
		UIntToString(*(data[i]), number);
		HAL_USCIA1_WriteString(number);
		HAL_USCIA1_WriteString(";");
	}
}

void DL_WLAN_SendHex(uint32_t **data, uint16_t length)
{
	int i;

	char number[11] = {0};
	for(i = 0; i < length; i++)
	{
		UIntToHex(*(data[i]), number);
		HAL_USCIA1_WriteString(number);
		HAL_USCIA1_WriteString(";");
	}
}

void DL_WLAN_SendFloat(float **data, uint16_t length)
{
	int i;

	char number[10] = {0};
	for(i = 0; i < length; i++)
	{
		FloatToString(number, *(data[i]), 5);
		HAL_USCIA1_WriteString(number);
		HAL_USCIA1_WriteString(";");
	}
}

void DL_WLAN_SendValues()
{
	DL_WLAN_SendInt16(Int16.variables, Int16.count);
	DL_WLAN_SendUInt16(UInt16.variables, UInt16.count);
	DL_WLAN_SendInt32(Int32.variables, Int32.count);
	DL_WLAN_SendUInt32(UInt32.variables, UInt32.count);
	DL_WLAN_SendFloat(Float.variables, Float.count);
	HAL_USCIA1_SendByte('\n');
}
