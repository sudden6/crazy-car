/*
 * driver_aktorik.h
 *
 *  Created on: 08.11.2016
 *      Author: hansi
 */

#ifndef DL_DL_AKTORIK_H_
#define DL_DL_AKTORIK_H_

#include <inttypes.h>

#define MAX_LEFT				-100
#define MAX_RIGHT				100
#define LEFT_US					1240
#define RIGHT_US				1800

#define REVERSE_US_MAX			1000
#define REVERSE_US_MIN			2000
#define FORWARD_US_MIN			3000
#define FORWARD_US_MAX			4000
#define BREAK_MAX				(((FORWARD_US_MIN - REVERSE_US_MIN) / 2) + REVERSE_US_MIN)

#define MAX_REVERSE				0
#define MAX_FORWARD				200

#define THROTTLE_MODE_UART		0
#define THROTTLE_MODE_PWM		1


void DL_Aktorik_SetSteering(int16_t val);
void DL_Aktorik_SetThrottle(int16_t speed);
void DL_Aktorik_InitThrottle(uint8_t mode);


#endif /* DL_DL_AKTORIK_H_ */
