/*
 * DL_distance.c
 *
 *  Created on: 12.12.2016
 *      Author: christoph
 */

#include <inttypes.h>
#include "../HAL/hal_adc12.h"
#include "../HAL/hal_timerA0.h"
#include "../HAL/hal_ucs.h"
#include "../util/crazycar_data.h"
#include "../util/tools.h"

// Constants to convert ADC values to distance
// calculated with sharp_sensor.m  for the
// formula found here: https://acroname.com/articles/linearizing-sharp-ranger-data
#define FRONT_K 12
#define FRONT_M 89711L
#define FRONT_B 89

#define RIGHT_K 21
#define RIGHT_M 132026L
#define RIGHT_B 379

#define LEFT_K 8
#define LEFT_M 70712L
#define LEFT_B -133

#define SENS_MAX 210

extern CaptureVal capture_val_2;

uint16_t DL_Sensorik_GetDistanceFront()
{

    int16_t measured = HAL_ADC12_GetChannelFiltered(ADC12_CHAN_FRONT);
    measured += FRONT_B;
    if(measured > (FRONT_M/(SENS_MAX + FRONT_K)))
    {
        return (FRONT_M /((uint32_t) measured) - FRONT_K)*10;
    }
    return SENS_MAX*10;
}

uint16_t DL_Sensorik_GetDistanceLeft()
{
    int16_t measured = HAL_ADC12_GetChannelFiltered(ADC12_CHAN_LEFT);
    measured += LEFT_B;
    if(measured > (LEFT_M/(SENS_MAX + LEFT_K)))
    {
        return (LEFT_M /((uint32_t) measured) - LEFT_K)*10;
    }
    return SENS_MAX*10;
}

uint16_t DL_Sensorik_GetDistanceRight()
{
    int16_t measured = HAL_ADC12_GetChannelFiltered(ADC12_CHAN_RIGHT);
    measured += RIGHT_B;
    if(measured > (RIGHT_M/(SENS_MAX + RIGHT_K)))
    {
        return (RIGHT_M /((uint32_t) measured) - RIGHT_K)*10;
    }
    return SENS_MAX*10;
}

// returns: Battery voltage in mV
uint16_t DL_Sensorik_GetVBat()
{
	uint16_t measured = HAL_ADC12_GetChannelFiltered(ADC12_CHAN_VBAT);
	uint32_t tmp = ((uint32_t)measured) * ADC_VREF * (VBAT_R1 + VBAT_R2);
	tmp = tmp / (VBAT_R1 * 0xFFFUL);

	return (uint16_t)tmp;
}

//returns the current speed of the car in mm/s
int32_t DL_Sensorik_GetSpeed()
{
	int32_t time_read = capture_val_2.speed;

	if(time_read == 0)
	{
		return 0;
	}
	int32_t speed = CIRCUMFERENCE_WHEEL * SMCLK_FREQU;
	speed /= time_read * NUMBER_MAGNETS;
	return speed;
}

//returns the covered distance of the car in mm
int32_t DL_Sensorik_GetCoveredDistance()
{
	//calculate covered distance in mm
	int32_t distance = (CIRCUMFERENCE_WHEEL * capture_val_2.covered_distance);
	distance /= (NUMBER_MAGNETS); //distance divided by the number of magnets

	return distance;
}


