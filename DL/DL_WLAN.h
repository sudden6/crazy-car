/*
 * DL_WLAN.h
 *
 *  Created on: 23.01.2017
 *      Author: hansi
 */

#ifndef DL_DL_WLAN_H_
#define DL_DL_WLAN_H_

void DL_WLAN_RegisterUInt16(uint16_t *ptr);
void DL_WLAN_RegisterUInt32(uint32_t *ptr);
void DL_WLAN_RegisterInt16(int16_t *ptr);
void DL_WLAN_RegisterInt32(int32_t *ptr);
void DL_WLAN_RegisterFloat(float *ptr);
void DL_WLAN_SendInt16(int16_t **data, uint16_t length);
void DL_WLAN_SendInt32(int32_t **data, uint16_t length);
void DL_WLAN_SendUInt16(uint16_t **data, uint16_t length);
void DL_WLAN_SendUInt32(uint32_t **data, uint16_t length);
void DL_WLAN_SendHex(uint32_t **data, uint16_t length);
void DL_WLAN_SendFloat(float **data, uint16_t length);
void DL_WLAN_Init();
void DL_WLAN_SendValues();

#endif /* DL_DL_WLAN_H_ */
