#include <msp430.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

#include "DL/DL_aktorik.h"
#include "DL/DL_general.h"
#include "util/tools.h"
#include "HAL/hal_general.h"
#include "HAL/hal_gpio.h"
#include "HAL/hal_usciB1.h"
#include "HAL/hal_usciA0.h"
#include "HAL/hal_adc12.h"
#include "HAL/hal_timerA0.h"
#include "DL/DL_display.h"
#include "DL/DL_sensorik.h"
#include "util/controller.h"
#include "DL/DL_WLAN.h"
#include "HAL/hal_usciA1.h"
#include "util/lowpass.h"
#include "util/ringbuffer.h"
#include "util/edge_detect.h"


#define MAIN_DELAY 5
#define CURVE_LEAVE_ANGLE 45

extern volatile ButtonCom buttons;

enum States{FORWARD = 0, CURVE_RIGHT = 1, CURVE_LEFT = 2, NOTHING = 3, STUCK = 4, CIRCLE = 5, WRONG_DIRECTION = 6};

static void InitControllers();
static void Statemachine();
static void reverseDirection();

static PIDContr throt_steer[2];
struct SensorData
{
	uint16_t distanceFront;
	uint16_t distanceLeft;
	uint16_t distanceRight;
	int32_t speed;
	int32_t distanceWheel;
};

struct ActorData
{
	int16_t speed;
	int16_t steer;
};

struct EdgeData
{
	int front;
	int left;
	int right;
};

static struct SensorData sensors;
static struct ActorData actors;
static struct EdgeData edges;

#define SENSOR_BUF_SIZE 32

static uint16_t front_values[SENSOR_BUF_SIZE] = {0};
static uint16_t left_values[SENSOR_BUF_SIZE] = {0};
static uint16_t right_values[SENSOR_BUF_SIZE]= {0};
static uint16_t dist_values[SENSOR_BUF_SIZE] = {0};

static struct UT_RingBufferU16 front_ring;
static struct UT_RingBufferU16 left_ring;
static struct UT_RingBufferU16 right_ring;
static struct UT_RingBufferU16 dist_ring;
uint16_t state = FORWARD;

uint8_t volatile update_pid = 0;
uint8_t drive = 0;
int32_t circle_count = 0;

int32_t dist_forward = 0;
float sin_ratio = 0.0;

void CalcSinRatio()
{
	float angle1 = PI / 180.0 * CURVE_LEAVE_ANGLE;
	float angle2 = PI / 180.0 * (135 - CURVE_LEAVE_ANGLE);

	sin_ratio = sinf(angle2) / sinf(angle1);
}

int main(void)
{
	//test comment


	HAL_Init();
	RED_ON;
	GREEN_ON;
	BLUE_ON;
    DL_Init();

    CalcSinRatio();

    int16_t e = (int16_t)throt_steer[0].parameter.e;
    int16_t ealt = (int16_t)throt_steer[0].parameter.ealt;
    int16_t esum = (int16_t)throt_steer[0].parameter.e;
    uint16_t left_adc = HAL_ADC12_GetChannel(ADC12_CHAN_RIGHT);
    // register debug output

    HAL_USCIA1_WriteString("throttle;");
    DL_WLAN_RegisterInt16(&(actors.speed));
    HAL_USCIA1_WriteString("steering;");
    DL_WLAN_RegisterInt16(&(actors.steer));
    HAL_USCIA1_WriteString("edgeLeft;");
    DL_WLAN_RegisterInt16(&(edges.left));
    HAL_USCIA1_WriteString("edgeRight;");
    DL_WLAN_RegisterInt16(&(edges.right));
    //DL_WLAN_RegisterInt16(&e);
    //DL_WLAN_RegisterInt16(&ealt);
    //DL_WLAN_RegisterInt16(&esum);
    HAL_USCIA1_WriteString("distanceFront;");
    DL_WLAN_RegisterUInt16(&(sensors.distanceFront));
    HAL_USCIA1_WriteString("distanceLeft;");
    DL_WLAN_RegisterUInt16(&(sensors.distanceLeft));
    HAL_USCIA1_WriteString("distanceRight;");
    DL_WLAN_RegisterUInt16(&(sensors.distanceRight));
    HAL_USCIA1_WriteString("state;");
    DL_WLAN_RegisterUInt16(&state);
    //DL_WLAN_RegisterFloat(&throt_steer[0].parameter.e);
    //DL_WLAN_RegisterFloat(&throt_steer[0].parameter.ealt);
    //DL_WLAN_RegisterFloat(&throt_steer[0].parameter.esum);

    HAL_USCIA1_WriteString("speed;");
    DL_WLAN_RegisterInt32(&(sensors.speed));
    //DL_WLAN_RegisterInt32(&(sensors.distanceWheel));

    HAL_USCIA1_WriteString("\n");
    InitControllers();
    HAL_TimerA0_HandlePIDController(&update_pid);

    // initialise ring buffers
    UT_RingBufferU16_Init(&front_ring, front_values, SENSOR_BUF_SIZE);
    UT_RingBufferU16_Init(&left_ring, left_values, SENSOR_BUF_SIZE);
    UT_RingBufferU16_Init(&right_ring, right_values, SENSOR_BUF_SIZE);
    UT_RingBufferU16_Init(&dist_ring, dist_values, SENSOR_BUF_SIZE);

	sensors.distanceFront = DL_Sensorik_GetDistanceFront();
	sensors.distanceLeft  = DL_Sensorik_GetDistanceLeft();
	sensors.distanceRight = DL_Sensorik_GetDistanceRight();
	sensors.speed 		  = DL_Sensorik_GetSpeed();
	sensors.distanceWheel = DL_Sensorik_GetCoveredDistance();


	// preload ring buffers
	UT_RingBufferU16_InsertOvw(&front_ring, sensors.distanceFront);
	UT_RingBufferU16_InsertOvw(&left_ring, sensors.distanceLeft);
	UT_RingBufferU16_InsertOvw(&right_ring, sensors.distanceRight);
	UT_RingBufferU16_InsertOvw(&dist_ring, sensors.distanceWheel);


    while(1)
    {
    	RED_OFF;
    	GREEN_OFF;
    	BLUE_OFF;

    	if(START_BUTTON_STATE)
    	{
    		drive = 1;
        	DL_Aktorik_SetThrottle(100);
        	DL_Aktorik_SetSteering(0);
			ResetPID(&throt_steer[0]);
			actors.speed = 0;
			actors.steer = 0;
    	}
    	else if(STOP_BUTTON_STATE)
    	{
    		drive = 0;
        	DL_Aktorik_SetThrottle(0);
        	DL_Aktorik_SetSteering(0);
			ResetPID(&throt_steer[0]);
			actors.speed = 0;
			actors.steer = 0;
    	}

    	if(!drive)
    	{
    		continue;
    	}

    	sensors.distanceFront = DL_Sensorik_GetDistanceFront();
    	sensors.distanceLeft  = DL_Sensorik_GetDistanceLeft();
    	sensors.distanceRight = DL_Sensorik_GetDistanceRight();
    	sensors.speed 		  = DL_Sensorik_GetSpeed();
    	sensors.distanceWheel = DL_Sensorik_GetCoveredDistance();
    	left_adc = HAL_ADC12_GetChannel(ADC12_CHAN_RIGHT);


		// update ring buffers
    	UT_RingBufferU16_InsertOvw(&front_ring, sensors.distanceFront);
    	UT_RingBufferU16_InsertOvw(&left_ring, sensors.distanceLeft);
    	UT_RingBufferU16_InsertOvw(&right_ring, sensors.distanceRight);
    	UT_RingBufferU16_InsertOvw(&dist_ring, sensors.distanceWheel);

		// run edge detection
		edges.left  = UT_Edge_Detect(&left_ring, 7, 450);
		edges.right = UT_Edge_Detect(&right_ring, 7, 300);

    	Statemachine();

		throt_steer[0].istwert = sensors.speed;
		int16_t diff = (int16_t)sensors.distanceLeft - sensors.distanceRight;
	    throt_steer[1].istwert = (int32_t)diff;
		HandleControllers(throt_steer, 2);
		if(throt_steer[0].enabled)
		{
			actors.speed = (int16_t)throt_steer[0].y;
		}

		if(throt_steer[1].enabled)
		{
			actors.steer = (int16_t)throt_steer[1].y;
		}

    	DL_Aktorik_SetThrottle(actors.speed);
    	DL_Aktorik_SetSteering(actors.steer);

    	DL_WLAN_SendValues();

	    /*
		if(diff > 0)
		{
		  actors.steer = -100;
		}
		else
		{
		   actors.steer = 100;
		}
		//*/


    	_delay_ms(MAIN_DELAY);

    }

	return 0;
}


void InitControllers()
{
	//PID-Controller Throttle
	throt_steer[0].enabled = 1;
	throt_steer[0].sollwert = 0;
	throt_steer[0].parameter.ta = 1;
	throt_steer[0].parameter.kp = 0.06; //prev 0.03
	throt_steer[0].parameter.ki = 0.004; //prev 0.005
	throt_steer[0].parameter.kd = 0.0005; //prev 0.005
	throt_steer[0].parameter.satLow = -100;
	throt_steer[0].parameter.satUp = 100;
	throt_steer[0].y = 0;

	//PID-Controller Steering
	throt_steer[1].enabled = 1;
	throt_steer[1].sollwert = 0;
	throt_steer[1].parameter.ta = 1;
	throt_steer[1].parameter.kp = 0.08;	// 0.05 works, 0.07 is good, 0.1 is the upper limit
	throt_steer[1].parameter.ki = 0;
	throt_steer[1].parameter.kd = 0;
	throt_steer[1].parameter.satLow = -100;
	throt_steer[1].parameter.satUp = 100;
	throt_steer[1].y = 0;
}

int prevState = 0;
static void Statemachine()
{
#define FRONT_DISTANCE_STUCK 250
#define CURVE_SPEED 900
#define CURVE_END_DIST_SIDE 650
#define CURVE_END_SIDE_MIN 460
#define CURVE_END_DIST_FRONT 900
#define CURVE_DETECT_DIST 1000
#define FORWARD_SPEED  3500
	// #define BREAK_DISTANCE 1400 // low battery
#define BREAK_DISTANCE 1800
#define BREAK_SPEED 1000

#define DISTANCE_UNSTUCK 350
#define REVERSE_SPEED -75
#define UNSTUCK_STEER 100
#define STUCK_COUNT 20

#define LOOP_DETECT 220

#define STRAIGHT_DIST 4500

    RED_OFF;
    GREEN_OFF;
    BLUE_OFF;

    /*
	uint16_t distanceForwardLeft = (sensors.distanceLeft * 22) /10; // Faktor 19 ~30°, kleiner spitzerer winkel, größer steilerer Winkel
	uint16_t distanceForwardRight = (sensors.distanceRight * 22)/10;
	uint16_t forwardCorrected = sensors.distanceFront + 0;			// add distance between left/right sensors and front sensor
	//*/

    uint16_t distanceForwardLeft = (uint16_t)((float)sensors.distanceLeft * sin_ratio);  // winkel zwischen
    uint16_t distanceForwardRight = (uint16_t)((float)sensors.distanceRight * sin_ratio);
    uint16_t forwardCorrected = sensors.distanceFront + 0;			// add distance between left/right sensors and front sensor

	static uint16_t stuck_counter = 0;
	int16_t diff = (int16_t)sensors.distanceLeft - sensors.distanceRight;

	// stuck detection
	if((state != WRONG_DIRECTION) && ((sensors.speed == 0) || (sensors.distanceFront < FRONT_DISTANCE_STUCK)))
	{
		stuck_counter++;
		if(stuck_counter >= STUCK_COUNT)
		{
			state = STUCK;
		}
	}
	else
	{
		stuck_counter = 0;
	}

	// circle detection
	if(abs(circle_count) > LOOP_DETECT)
	{
		circle_count = 0;
		state = CIRCLE;
	}

	// Wrong direction detection

	/*
	// track distance in FORWARD_STATE
	if(state == FORWARD && prevState != FORWARD)
	{
		dist_forward = sensors.distanceWheel;
	}
	// on change from FORWARD to CURVE_RIGHT do wrong direction checking
	else if(state == CURVE_RIGHT && prevState == FORWARD)
	{

		if((sensors.distanceWheel - dist_forward) > STRAIGHT_DIST)
		{
			state = WRONG_DIRECTION;

			TOGGLE(LCD_BL);
			_delay_ms (100);
		}
		else
		{
			dist_forward = sensors.distanceWheel;
		}
	}
	// if we're not in FORWARD state, reset the distance counter
	else if(state != FORWARD)
	{
		dist_forward = sensors.distanceWheel;
	}
	//*/
	prevState = state;

	// state machine
	switch(state)
	{
		case CURVE_LEFT:
			// reset circle detection
			if(circle_count < 0)
			{
				circle_count = 0;
			}
			circle_count++;

			throt_steer[1].enabled = 0;
			actors.steer = -100;
			throt_steer[0].sollwert = CURVE_SPEED;
			//if((sensors.distanceLeft < CURVE_END_DIST_SIDE)&&
			//   (sensors.distanceFront < CURVE_END_DIST_FRONT))
			if((sensors.distanceLeft < CURVE_END_DIST_SIDE) &&
					(forwardCorrected > distanceForwardLeft))
			{
				throt_steer[1].enabled = 1;
				state = FORWARD;
				return;
			}/*
			if(sensors.distanceLeft < CURVE_END_SIDE_MIN)
			{
				throt_steer[1].enabled = 1;
				state = FORWARD;
				return;
			}*/

			break;
		case CURVE_RIGHT:
			// reset circle detection
			if(circle_count > 0)
			{
				circle_count = 0;
			}
			circle_count--;

			throt_steer[1].enabled = 0;
			actors.steer = 100;
			throt_steer[0].sollwert = CURVE_SPEED;
			//if((sensors.distanceLeft < CURVE_END_DIST_SIDE)&&
			//   (sensors.distanceFront < CURVE_END_DIST_FRONT))
			if((sensors.distanceRight < CURVE_END_DIST_SIDE) &&
					(forwardCorrected > distanceForwardRight) && (sensors.distanceFront > sensors.distanceLeft))
			{
				throt_steer[1].enabled = 1;
				state = FORWARD;
				return;
			}
			/*
			if(sensors.distanceRight < CURVE_END_SIDE_MIN)
			{
				throt_steer[1].enabled = 1;
				state = FORWARD;
				return;
			}*/

			break;

		case FORWARD:
			circle_count = 0;

			if(sensors.distanceFront < BREAK_DISTANCE)
			{
				throt_steer[0].sollwert = BREAK_SPEED;
			}
			else
			{
				throt_steer[0].sollwert = FORWARD_SPEED;
			}

			// curves with edge detection

			if(edges.left == 1)
			{
				BLUE_ON;
				state = CURVE_LEFT;
				return;
			}
			if(edges.right == 1)
			{
				GREEN_ON;
				state = CURVE_RIGHT;
				return;
			}

			//hack-curve detection
			/*
			if((sensors.distanceFront < CURVE_DETECT_DIST) && (sensors.distanceFront > 350))
			{
				if((sensors.distanceRight > 700) && (sensors.distanceLeft < 600) && (sensors.distanceLeft > 200)) // 800 & 600
				{
	                RED_ON;
					state = CURVE_RIGHT;
				}
				else if((sensors.distanceLeft > 700) && (sensors.distanceRight > 600) && (sensors.distanceRight > 200)) // 780 & 550
				{
					RED_ON;
					state = CURVE_LEFT;
				}
				return;
			}
			//*/
			/* backup curve algorithm
			else if(sensors.distanceFront < 600)
			{
				if(sensors.distanceLeft < sensors.distanceRight)
				{
					state = CURVE_RIGHT;
				}
				else
				{
					state = CURVE_LEFT;
				}
				return;
			}
			//*/
			break;
		case NOTHING:
			actors.speed = 0;
			break;

		case STUCK:
			circle_count = 0;

			stuck_counter = 0;
			// disable PID controllers
			throt_steer[0].enabled = 0;
			throt_steer[1].enabled = 0;
			if(sensors.distanceFront < DISTANCE_UNSTUCK)
			{
				if(diff > 0)
				{
					// links mehr platz als rechts
					actors.steer = UNSTUCK_STEER;
					actors.speed = REVERSE_SPEED;
				}
				else
				{
					// rechts mehr platz als links
					actors.steer = -UNSTUCK_STEER;
					actors.speed = REVERSE_SPEED;
				}
			}
			else
			{
				state = FORWARD;
				ResetPID(&throt_steer[0]);
				// enable PID controllers
				throt_steer[0].enabled = 1;
				throt_steer[1].enabled = 1;
			}
			break;

		case CIRCLE:
	    	DL_Aktorik_SetThrottle(0);
	    	DL_Aktorik_SetSteering(0);
	    	_delay_ms(300);
	    	DL_Aktorik_SetThrottle(50);
	    	_delay_ms(1000);

	    	state = FORWARD;
	    	break;

		case WRONG_DIRECTION:
			DL_Sensorik_GetCoveredDistance();
			// disable PID controllers
			throt_steer[0].enabled = 0;
			throt_steer[1].enabled = 0;
			actors.steer = 0;
			actors.speed = 0;

			reverseDirection();
			state = FORWARD;
			ResetPID(&throt_steer[0]);
			// enable PID controllers
			throt_steer[0].enabled = 1;
			throt_steer[1].enabled = 1;
			break;

		default:
			break;

	}
}

void reverseDirection()
{
#define REVERSE_THROTTLE 80
#define REVERSE_TIME_1	750
#define REVERSE_TIME_2	900
#define REVERSE_TIME_3	1100

	// break
	DL_Aktorik_SetThrottle(-10);
	_delay_ms(500);

	// Phase 1 Drive forward left against the wall
	DL_Aktorik_SetSteering(-100);
	DL_Aktorik_SetThrottle(REVERSE_THROTTLE);
	_delay_ms(REVERSE_TIME_1);
	DL_Aktorik_SetThrottle(0);	// Stop
	_delay_ms(100);

	// Phase 2 Drive backward right
	DL_Aktorik_SetSteering(100);
	DL_Aktorik_SetThrottle(-REVERSE_THROTTLE);
	_delay_ms(REVERSE_TIME_2);
	DL_Aktorik_SetThrottle(0);	// Stop
	_delay_ms(100);

	// Phase 1 Drive forward left in the other direction
	DL_Aktorik_SetSteering(-100);
	DL_Aktorik_SetThrottle(REVERSE_THROTTLE);
	_delay_ms(REVERSE_TIME_3);
	DL_Aktorik_SetThrottle(0);	// Stop
	_delay_ms(100);
}
