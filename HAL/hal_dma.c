/*
 * hal_dma.c
 *
 *  Created on: 06.12.2016
 *      Author: hansi
 */
#include <msp430.h>
#include <inttypes.h>
#include "hal_adc12.h"
#include "../util/lowpass.h"

extern struct HAL_ADC12 adc;

//initializes the DMA-Block for the ADC12
void HAL_DMA_Init()
{
	DMA0CTL |= DMADT_4; 					//Repeated block transfer
	DMA0CTL |= DMADSTINCR_3;				//Increment destination address
	DMA0CTL |= DMASRCINCR_3;				//Increment source address
	DMA0CTL |= DMAIE;						//Enable Interrupt
	DMA0CTL &= ~DMAIFG;						//Delete Interrupt-flag if it is pending
	DMACTL0 |= DMA0TSEL_24;					//Use Interrupt-Flag of ADC12
	DMA0SA =  (void (*)())&ADC12MEM0;		//Source address = ADC12 Memory Register
	DMA0DA =  (void (*)())adc.Buffer;		//Destination address = Buffer of ADC
	DMA0SZ = 5;								//5 words to transfer

	DMA0CTL |= DMAEN;		//Enable DMA
}

static void HAL_DMA_ExecuteFilter()
{
	int i = 0;
	for(; i < ADC12_CHAN_NUM_CHANNELS; i++)
	{
		adc.Filtered[i] = lowpass(&adc.FilterBuffer[i], adc.Buffer[i]);
	}
}

#pragma vector=DMA_VECTOR
__interrupt void DMA_ISR(void)
{
	if(DMAIV & DMAIV_DMA0IFG) //DMA finished transfer
	{
		HAL_DMA_ExecuteFilter();
		adc.Rdy = 1;	//new ADC-values are available
	}
}
