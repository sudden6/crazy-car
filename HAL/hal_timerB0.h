/*
 * hal_timerB0.h
 *
 *  Created on: 25.10.2016
 *      Author: hansi
 */

#ifndef HAL_HAL_TIMERB0_H_
#define HAL_HAL_TIMERB0_H_

#define HAL_TIMERB0_FREQ 120

void HAL_TimerB0_Init();


#endif /* HAL_HAL_TIMERB0_H_ */
