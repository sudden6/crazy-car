/*
 * hal_usciA0.h
 *
 *  Created on: 16.11.2016
 *      Author: hansi
 */

#ifndef HAL_HAL_USCIA1_H_
#define HAL_HAL_USCIA1_H_

#include <inttypes.h>


//Configure UART-Parameters
#define HAL_USCIA1_BAUDRATE 		115200
#define HAL_USCIA1_SND_BUF_SIZE		256
#define HAL_USCIA1_REC_BUF_SIZE		256

//init
void HAL_USCIA1_Init();

//transmitting
int8_t HAL_USCIA1_SendByte(uint8_t byte);
int8_t HAL_USCIA1_SendBytes(uint8_t *data, uint16_t length);
int8_t HAL_USCIA1_WriteString(char *text);

//receiving
uint8_t HAL_USCIA1_GetByte();
int8_t HAL_USCIA1_GetBytes(uint8_t *to_store, uint16_t length);

//status
inline uint16_t HAL_USCIA1_AvailableBytes();
inline uint8_t HAL_USCIA1_ReceiveBufferOverflow();

//flushing
void HAL_USCIA1_FlushRxBuffer();
void HAL_USCIA1_FlushTxBuffer();

#endif /* HAL_HAL_USCIA1_H_ */
