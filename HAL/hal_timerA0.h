
#ifndef HAL_HAL_TIMERA0_H_
#define HAL_HAL_TIMERA0_H_

#include <inttypes.h>
#include "../util/controller.h"

typedef struct
{
	int32_t covered_distance;	//covered distance of the car
	int32_t speed;				//speed of the car
}CaptureVal;

void HAL_TimerA0_Init();
void HAL_TimerA0_HandlePIDController(uint8_t volatile *update);

#endif
