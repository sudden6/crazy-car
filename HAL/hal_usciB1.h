/*
 * hal_usciB1.h
 *
 *  Created on: 16.11.2016
 *      Author: christoph
 */

#ifndef HAL_HAL_USCIB1_H_
#define HAL_HAL_USCIB1_H_

#include <inttypes.h>


#define HAL_USCIB1_SPI_BUF_SIZE		256				// size of the SPI send and receive buffer, RAM usage is twice this value
#define HAL_USCIB1_SPI_CLOCK 		100000			// clock frequency of the SPI interface

void HAL_USCIB1_Init();
void HAL_USCIB1_SendByte(uint8_t byte);
int8_t HAL_USCIB1_SendBytes(uint8_t* data, uint16_t len);
uint16_t HAL_USCIB1_AvailableBytes();
uint16_t HAL_USCIB1_Busy();
uint8_t HAL_USCIB1_GetByte();
int8_t HAL_USCIB1_GetBytes(uint8_t *target, uint16_t len);
int8_t HAL_USCIB1_SendByteWait(uint8_t byte);
int8_t HAL_USCIB1_SendBytesWait(uint8_t* send, uint8_t* received, uint16_t len);

#endif /* HAL_HAL_USCIB1_H_ */
