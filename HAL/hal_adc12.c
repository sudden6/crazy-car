/*
 * hal_adc12.c
 *
 *  Created on: 29.11.2016
 *      Author: christoph
 */

#include <inttypes.h>
#include <msp430.h>
#include "hal_adc12.h"

struct HAL_ADC12 adc;

void HAL_ADC12_Init()
{
	REFCTL0   &= ~REFMSTR;		// reference voltage is controlled by ADC
	ADC12CTL0 = ADC12ON			// enable ADC block
			  | ADC12REFON		// enable ADC reference voltage
			  | ADC12REF2_5V	// set reference voltage to 2.5V
			  | ADC12MSC		// trigger conversion series at once
			  | ADC12SHT0_2		// sample time = 6,4us
			  | ADC12SHT1_2;	// sample time = 6,4us

	ADC12CTL1 = ADC12SSEL_3		// use SMCLK for ADC clock
			  // use TimerB0CCR1 as trigger,
			  // see Table 6-15. Timer TB0 Signal Connections in the device specific datasheet
			  | ADC12SHS_3
			  | ADC12SHP		// use automatic sample and hold time
			  | ADC12CONSEQ_3;	// sample all channels in sequence

	ADC12CTL2 = ADC12RES_2;		// 12Bits resolution

	// configure sampling sequence
	// Distance Right, pin A0
	ADC12MCTL0 = ADC12SREF_1	// Vref- = GND, Vref+ = 2.5V
			   | ADC12INCH_0;	// pin A0
	// Distance Left, pin A1
	ADC12MCTL1 = ADC12SREF_1	// Vref- = GND, Vref+ = 2.5V
			   | ADC12INCH_1;	// pin A1
	// Distance Front, pin A2
	ADC12MCTL2 = ADC12SREF_1	// Vref- = GND, Vref+ = 2.5V
			   | ADC12INCH_2;	// pin A2
	// VBat, pin A3
	ADC12MCTL3 = ADC12SREF_1	// Vref- = GND, Vref+ = 2.5V
			   | ADC12INCH_3;	// pin A3
	// Internal Temperature
	ADC12MCTL4 = ADC12SREF_1	// Vref- = GND, Vref+ = 2.5V
			   | ADC12INCH_10	// Internal Temperature Diode
			   | ADC12EOS;		// End of sequence

	// Enable Interrupt when last conversion is done
	//ADC12IE = ADC12IE4; //disable for DMA!
	ADC12IFG = 0;
	ADC12CTL0 |= ADC12ENC;
}

uint16_t HAL_ADC12_GetChannel(enum HAL_ADC12_CHANNELS chan)
{
	if(chan < ADC12_CHAN_NUM_CHANNELS)
	{
		return adc.Buffer[chan];
	}
	else
	{
		return 0;
	}
}

uint16_t HAL_ADC12_GetChannelFiltered(enum HAL_ADC12_CHANNELS chan)
{
	if(chan < ADC12_CHAN_NUM_CHANNELS)
	{
		return adc.Filtered[chan];
	}
	else
	{
		return 0;
	}
}
