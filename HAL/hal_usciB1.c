/*
 * hal_usciB1.c
 *
 *  Created on: 16.11.2016
 *      Author: christoph
 */

#include <msp430.h>
#include "hal_usciB1.h"
#include "hal_ucs.h"

typedef struct
{
	uint16_t volatile len; 						// number of bytes to send
	uint16_t volatile rx_head; 					// index of the next free byte in the read buffer
	uint16_t volatile rx_tail;					// index of the next byte to read in the read buffer
	uint8_t data[HAL_USCIB1_SPI_BUF_SIZE];		// Tx Daten Array
} USCIB1_SPICom;

volatile USCIB1_SPICom spi_buffer;

void HAL_USCIB1_Init()
{
	// SPI Master Mode
	// 100 kHz clock frequency => defined in header-file
	// 8 bit, MSB first
	// Clock Phase: 0
	// Clock Polarity: 1

	UCB1CTL1 = UCSSEL__SMCLK|UCSWRST;						// set clock to SMCLK, start software reset
	UCB1CTL0 = UCCKPL|UCMST|UCSYNC|UCMSB;					// setup polarity, phase, SPI mode, MSB first
	UCB1BR0  = SMCLK_FREQU / HAL_USCIB1_SPI_CLOCK;			// set low byte of prescaler
	UCB1BR1  = (SMCLK_FREQU / HAL_USCIB1_SPI_CLOCK) / 256;	// set high byte of prescaler
	UCB1CTL1 &= ~UCSWRST;									// disable software reset
	UCB1IE   = UCRXIE;										// enable RX-Interrupt
}

// starts transmitting spi_buffer.len bytes in spi_buffer.TxData D
static void StartTransmit()
{
	spi_buffer.rx_head = 0;
	spi_buffer.rx_tail = 0;
	UCB1TXBUF = spi_buffer.data[0];	// put first byte in TXBUF, rest is handled by ISR
}

// transmits one byte over SPI, overwrites previous buffer if there's data in it
// byte: byte to send
void HAL_USCIB1_SendByte(uint8_t byte)
{
	spi_buffer.len = 1;
	spi_buffer.data[0] = byte;
	StartTransmit();
}

// transmits len bytes over SPI, overwrites previous buffer if there's data in it
// data   : pointer to data to send
// len    : number of bytes to send
// returns: 0 on success, -1 on error
int8_t HAL_USCIB1_SendBytes(uint8_t* data, uint16_t len)
{
	// check if data fits in the buffer
	if(len > HAL_USCIB1_SPI_BUF_SIZE)
	{
		return -1;
	}

	spi_buffer.len = len;

	// copy data to buffer
	uint16_t write_pos;
	for(write_pos = 0; write_pos < len; write_pos++)
	{
		spi_buffer.data[write_pos] = data[write_pos];
	}

	// begin transmission
	StartTransmit();
	return 0;
}

// get the number of bytes available in the spi buffer
// returns: number of bytes in receive buffer
uint16_t HAL_USCIB1_AvailableBytes()
{
	return spi_buffer.rx_head - spi_buffer.rx_tail;
}

// check if the spi interface is still sending/receiving
// returns: >0 if busy, 0 if not busy
uint16_t HAL_USCIB1_Busy()
{
	// check if the last byte has been received
	return spi_buffer.rx_head < spi_buffer.len;
}

// reads a byte from the internal buffer
static uint8_t ReadBufferByte()
{
	// read the current byte
	uint8_t byte = spi_buffer.data[spi_buffer.rx_tail];
	// point to the next byte
	spi_buffer.rx_tail++;
	return byte;
}

// reads a byte from the receive buffer
// returns: next byte from the buffer, 0 if no more bytes are available
uint8_t HAL_USCIB1_GetByte()
{
	// check if bytes are available
	if(HAL_USCIB1_AvailableBytes() > 0)
	{
		return ReadBufferByte();
	}
	else
	{
		// no more bytes to read
		return 0;
	}
}

// try to read len bytes from the receive buffer
// target : destination buffer for the data
// len    : number of bytes to read
// returns: -1 if not enough bytes are available, 0 on success
int8_t HAL_USCIB1_GetBytes(uint8_t *dest, uint16_t len)
{
	// check if enough bytes are available
	if(HAL_USCIB1_AvailableBytes() < len)
	{
		// not enough bytes to read
		return -1;
	}
	// copy data to the destination
	uint16_t read_pos;
	for(read_pos = 0; read_pos < len; read_pos++)
	{
		dest[read_pos] = ReadBufferByte();
	}
	return 0;
}

// write one byte and block while receiving the answer byte
// byte   : byte to send
// returns: byte read from SPI, 0 on error
int8_t HAL_USCIB1_SendByteWait(uint8_t byte)
{
	HAL_USCIB1_SendByte(byte);
	while(HAL_USCIB1_Busy())
	{
		// wait for receive complete
	}
	return HAL_USCIB1_GetByte();
}

// write a number of bytes and block while receiving the answer bytes
// the number of sent and received bytes is always the same
// send and received buffer can be the same
// send    : pointer to the data to send
// received: pointer where the received data should be saved
// len     : number of bytes to send and receive
// returns : 0 on success, <0 on error
int8_t HAL_USCIB1_SendBytesWait(uint8_t* send, uint8_t* received, uint16_t len)
{
	if(!HAL_USCIB1_SendBytes(send, len))
	{
		return -1;
	}
	while(HAL_USCIB1_Busy())
	{
		// wait....
	}
	return HAL_USCIB1_GetBytes(received, len);
}

#pragma vector=USCI_B1_VECTOR
__interrupt void USCI_B1_ISR (void)
{
	spi_buffer.data[spi_buffer.rx_head] = UCB1RXBUF;			// read data from RX-buffer
	spi_buffer.rx_head++;										// increase position

	if(spi_buffer.rx_head < spi_buffer.len)						// check if last byte received
	{
		UCB1TXBUF = spi_buffer.data[spi_buffer.rx_head];		// send next byte
	}
}
