/*
 * hal_usciA0.h
 *
 *  Created on: 16.11.2016
 *      Author: hansi
 */

#ifndef HAL_HAL_USCIA0_H_
#define HAL_HAL_USCIA0_H_

#include <inttypes.h>


//Configure UART-Parameters
#define HAL_USCIA0_BAUDRATE 		9600
#define HAL_USCIA0_SND_BUF_SIZE		256
#define HAL_USCIA0_REC_BUF_SIZE		256

//init
void HAL_USCIA0_Init();

//transmitting
int8_t HAL_USCIA0_SendByte(uint8_t byte);
int8_t HAL_USCIA0_SendBytes(uint8_t *data, uint16_t length);
int8_t HAL_USCIA0_WriteString(char *text);

//receiving
uint8_t HAL_USCIA0_GetByte();
int8_t HAL_USCIA0_GetBytes(uint8_t *to_store, uint16_t length);

//status
inline uint16_t HAL_USCIA0_AvailableBytes();
inline uint8_t HAL_USCIA0_ReceiveBufferOverflow();

//flushing
void HAL_USCIA0_FlushRxBuffer();
void HAL_USCIA0_FlushTxBuffer();

#endif /* HAL_HAL_USCIA0_H_ */
