/*
 * hal_usciA0.c
 *
 *  Created on: 16.11.2016
 *      Author: hansi
 */
#include <stdio.h>
#include <msp430.h>
#include "hal_usciA1.h"
#include "hal_ucs.h"


//DO NOT CHANGE!
#define EVEN			1
#define ODD				0


//Configure UART-Parameters
#define PARITY_ENABLE	0
#define PARITY			ODD

typedef struct
{
	struct
	{
		uint8_t volatile *rx_user;			//pointer to the current position for the user-output
		uint8_t volatile *rx_isr;			//pointer to the current position of the interrupt
		uint8_t volatile buffer_overflow;	//displays if a buffer-overflow happened
		uint8_t rx_buf[HAL_USCIA1_REC_BUF_SIZE]; 		//Rx Daten Array
	}RxData;

	struct
	{
		uint8_t volatile *tx_user;			//pointer to the current position of the user-input
		uint8_t volatile *tx_isr;			//pointer to the current position of the interrupt
		uint8_t tx_buf[HAL_USCIA1_SND_BUF_SIZE]; 		//Tx Daten Array
	}TxData;
}USCIA0_UARTCom;


enum check {TX_USER = 0, TX_ISR, RX_USER, RX_ISR};


static USCIA0_UARTCom uart_buf;


static uint16_t GetTxLength()
{
	if(uart_buf.TxData.tx_isr > uart_buf.TxData.tx_user)
	{
		return  (HAL_USCIA1_SND_BUF_SIZE + (uint16_t)(uart_buf.TxData.tx_user) - (uint16_t)(uart_buf.TxData.tx_isr));
	}
	else
	{
		return (uart_buf.TxData.tx_user - uart_buf.TxData.tx_isr);
	}
}

static inline uint16_t HAL_USCIA1_GetRxLength()
{
	if(uart_buf.RxData.rx_user > uart_buf.RxData.rx_isr)
	{
		return (HAL_USCIA1_REC_BUF_SIZE + uart_buf.RxData.rx_isr - uart_buf.RxData.rx_user);
	}
	else
	{
		return (uart_buf.RxData.rx_isr - uart_buf.RxData.rx_user);
	}
}

//checks if the pointer of a buffer has to get incremented or set to the first element
//type: which pointer has to be checked
static void CheckBufferPointer(uint16_t type)
{
	//checks if pointer points to last element of buffer
	//if yes set it to the first element, otherwise increment

	switch(type)
	{
		case TX_USER:
			if(uart_buf.TxData.tx_user == &uart_buf.TxData.tx_buf[HAL_USCIA1_SND_BUF_SIZE - 1])
			{
				//pointer has reached the last element of buffer and gets set to the first one
				uart_buf.TxData.tx_user = uart_buf.TxData.tx_buf;
			}
			else
			{
				//pointer gets incremented
				uart_buf.TxData.tx_user++;
			}
			break;

		case TX_ISR:
			if(uart_buf.TxData.tx_isr == &uart_buf.TxData.tx_buf[HAL_USCIA1_SND_BUF_SIZE - 1])
			{
				//pointer has reached the last element of buffer and gets set to the first one
				uart_buf.TxData.tx_isr = uart_buf.TxData.tx_buf;
			}
			else
			{
				//pointer gets incremented
				uart_buf.TxData.tx_isr++;
			}
			break;

		case RX_USER:
			uart_buf.RxData.buffer_overflow = 0; //if one byte was read, the buffer isn't full anymore

			if(uart_buf.RxData.rx_user == &uart_buf.RxData.rx_buf[HAL_USCIA1_REC_BUF_SIZE - 1])
			{
				//pointer has reached the last element of buffer and gets set to the first one
				uart_buf.RxData.rx_user = uart_buf.RxData.rx_buf;
			}
			else
			{
				//pointer gets incremented
				uart_buf.RxData.rx_user++;
			}
			break;

		case RX_ISR:
			if(uart_buf.RxData.rx_isr == &uart_buf.RxData.rx_buf[HAL_USCIA1_REC_BUF_SIZE -1])
			{
				//pointer has reached the last element of buffer and gets set to the first one
				uart_buf.RxData.rx_isr = uart_buf.RxData.rx_buf;
			}
			else
			{
				//pointer gets incremented
				uart_buf.RxData.rx_isr++;
			}
			break;

		default:
			break;
	}

}

//checks if the transmission of the send-buffer has already started
//length: number of bytes which were added to the send-buffer
static void StartTransmit(uint16_t length)
{
	UCA1IE |= UCTXIE;                         	// Enable USCI_A0 TX interrupt
}

//initializes the UART on the USCIA0-module
void HAL_USCIA1_Init()
{

	//Initialize Buffers
	HAL_USCIA1_FlushTxBuffer();
	HAL_USCIA1_FlushRxBuffer();


#if PARITY_ENABLE==1
	UCA1CTL0 |= UCPEN_1; //enable parity
#if PARITY == EVEN
	UCA1CTL0 |= UCPAR_1; //even parity
#endif
#endif

	UCA1CTL1 |= UCSWRST;                      	// **Put state machine in reset**
	UCA1CTL1 |= UCSSEL_2;						// SMCLK
	UCA1BR0 = 21;								// 2.5MHz 115200 see http://processors.wiki.ti.com/index.php/USCI_UART_Baud_Rate_Gen_Mode_Selection
	UCA1BR1 = 0;
	UCA1MCTL = UCBRS_6;
	UCA1CTL1 &= ~UCSWRST;                     	// **Initialize USCI state machine**


	UCA1IE |= UCRXIE;                         	// Enable USCI_A0 RX interrupt
}


//transmits one byte over UART
//byte : byte to send
//returns 0 on success, -1 on error
int8_t HAL_USCIA1_SendByte(uint8_t byte)
{
	if(GetTxLength() + 1 > HAL_USCIA1_SND_BUF_SIZE)
	{
		//buffer overflow
		return -1;
	}

	*uart_buf.TxData.tx_user = byte; //copy data into buffer

	CheckBufferPointer(TX_USER);

	StartTransmit(1);

	return 0;

}

//transmits an array of bytes over UART
//data: pointer to the first element of the array
//length: number of bytes which are to send from the array
//returns 0 on success, -1 on error
int8_t HAL_USCIA1_SendBytes(uint8_t *data, uint16_t length)
{
	if(GetTxLength() + length > HAL_USCIA1_SND_BUF_SIZE)
	{
		//buffer overflow
		return -1;
	}

	uint16_t i;
	for(i = 0; i < length; i++)
	{

		*uart_buf.TxData.tx_user = data[i]; //copy data into buffer

		CheckBufferPointer(TX_USER);
	}

	StartTransmit(length);

	return 0;
}

//transmits a char-array over UART
//text: pointer to the first element of the char[]
//returns 0 on success, -1 on error
int8_t HAL_USCIA1_WriteString(char *text)
{
	char *test = text;
	uint16_t len = 0;

	while((*test) != '\0')
	{
		//measure length of string
		test++;
		len++;
	}

	if(len + GetTxLength() > HAL_USCIA1_SND_BUF_SIZE)
	{
		//text too long for buffer
		return -1;
	}
	if(len == 0)
	{
		//empty string
		return 0;
	}

	while((*text) != '\0')
	{
		*uart_buf.TxData.tx_user = *text; 		//copy data into buffer
		text++; 								//increment pointer
		CheckBufferPointer(TX_USER);
	}

	StartTransmit(len);

	return 0;

}

//read one Byte from the UART-receive-buffer
//returns the received byte on success, on error returns 0
uint8_t HAL_USCIA1_GetByte()
{
	if(HAL_USCIA1_GetRxLength() == 0)
	{
		//no bytes available
		return 0;
	}

	uint8_t ret = *uart_buf.RxData.rx_user; //read byte from buffer

	CheckBufferPointer(RX_USER);

	return ret; //return the byte
}

//reads "length" of bytes out of the UART-receive-buffer
//to_store: pointer to the array where to save the read bytes
//length: number of bytes which have to be read
//returns 0 on success, -1 on error
int8_t HAL_USCIA1_GetBytes(uint8_t *to_store, uint16_t length)
{
	if(length > HAL_USCIA1_GetRxLength())
	{
		//not so many bytes in the buffer
		to_store = NULL;
		return -1;
	}

	uint16_t i;
	for(i = 0; i < length; i++)
	{
		to_store[i] = *uart_buf.RxData.rx_user; //read byte from buffer

		CheckBufferPointer(RX_USER);
	}

	return 0;
}

//returns the number of available bytes in the UART-receive-buffer
uint16_t HAL_USCIA1_AvailableBytes()
{
	return HAL_USCIA1_GetRxLength();
}

//returns 1 if a buffer-overflow happened, otherwise 0
inline uint8_t HAL_USCIA1_ReceiveBufferOverflow()
{
	return uart_buf.RxData.buffer_overflow;
}

//flushes the UART-receive-buffer
void HAL_USCIA1_FlushRxBuffer()
{
	//set buffer-pointer to first element of buffer and clear length
	uart_buf.RxData.rx_user = uart_buf.RxData.rx_buf;
	uart_buf.RxData.rx_isr = uart_buf.RxData.rx_buf;
	uart_buf.RxData.buffer_overflow = 0;
}

//flushes the UART-send-buffer
void HAL_USCIA1_FlushTxBuffer()
{
	//set buffer-pointer to first element of buffer and clear length
	uart_buf.TxData.tx_user = uart_buf.TxData.tx_buf;
	uart_buf.TxData.tx_isr = uart_buf.TxData.tx_buf;
}

#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
{
	if(UCA1IFG & UCTXIFG) 	//transmit-buffer is empty
	{
		if(GetTxLength() > 0) 		//length has to be more than 0
		{

			UCA1TXBUF = *uart_buf.TxData.tx_isr; 	//write byte into tx-buffer

			CheckBufferPointer(TX_ISR);
		}
		else
		{

			UCA1IE &= ~UCTXIE;		// Disable USCI_A0 TX interrupt
		}

	}
	if(UCA1IFG & UCRXIFG) //received a character
	{
		//if the length of the received bytes is smaller than the buffer-size, the length gets incremented
		//otherwise the buffer gets overwritten
		if(HAL_USCIA1_GetRxLength() < HAL_USCIA1_REC_BUF_SIZE)
		{
			uart_buf.RxData.buffer_overflow = 0; //no buffer-overflow
		}
		else
		{
			uart_buf.RxData.buffer_overflow = 1; //buffer-overflow
		}

		*uart_buf.RxData.rx_isr = UCA1RXBUF;	//value of uart-rx-buffer gets stored in ring-rx-buffer

		CheckBufferPointer(RX_ISR);

		UCA1IFG &= ~UCRXIFG; 					//delete the interrupt-flag
	}
}
