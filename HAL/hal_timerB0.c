#include <msp430.h>
#include "hal_ucs.h"
#include "hal_timerB0.h"
#include "hal_gpio.h"


void HAL_TimerB0_Init()
{
	// Timer clock 500kHz
	TB0CTL |= TBSSEL_2;		// use SMCLK
	TB0CTL |= ID__4;		// input Divider by 1
	TB0CTL |= MC_1;		 	// set to up-mode
	TB0CTL |= TBCLR;		// resets timer-register

	TB0EX0 |= TBIDEX__5;	// input divider by 5

	TB0CCR0 = (SMCLK_FREQU / (40 * HAL_TIMERB0_FREQ)) - 1; // ~120 Hz
	TB0CCR1 = 1;			// generate a short pulse for the ADC
	TB0CCTL1 |= OUTMOD_7;
}
