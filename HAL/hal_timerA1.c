#include <msp430.h>
#include <inttypes.h>
#include "hal_ucs.h"
#include "hal_timerA1.h"

uint16_t pulse_counter = 0;

// setup timerA1 to generate pwm signals for the steering servo
// and the motor controller
void HAL_TimerA1_Init()
{
	// PWM
	// Period = 1/60 s
	// Resolution = 2us

	// configure timer for 1MHz clock
	TA1CTL |= TASSEL__SMCLK;	//use SMCLK(2,5MHz)
	TA1EX0 |= TAIDEX_4;		// divide by 5
	TA1CCR0 = (SMCLK_FREQU / (5 * 60)) - 1; // 60Hz

	// configure compare units
	TA1CCTL1 = OUTMOD_7;		// set on 0 reset on CCR1
	TA1CCTL2 = OUTMOD_7;		// set on 0 reset on CCR2

	// enable the timer
	TA1CTL |= TBCLR;		//resets timer-register and dividers to 0
	TA1CTL |= MC__UP;		// set to up-mode
}

// sets the pulse length of the pwm output to length_us microseconds
void HAL_TimerA1_SetPulseLength(uint16_t length_us, uint16_t channel)
{
	switch(channel)
	{
	case CHANNEL_STEERING:
		TA1CCR2 = length_us / 2;
		break;
	case CHANNEL_THROTTLE:
		TA1CCR1 = length_us / 2;
		break;
	}
}

