/*
 * hal_ucs.c
 *
 *  Created on: 25.10.2016
 *      Author: christoph
 */

#include <msp430.h>

void HAL_UCS_Init()
{
	UCSCTL3 = SELREF__REFOCLK;				// FLLref = REFO, to avoid oscillator fault flag XT1OFFG being set
	UCSCTL4 = SELA__REFOCLK | SELS__REFOCLK | SELM__REFOCLK ;				// ACLK = MCLK = SMCLK = REFO

	// set drive strength for 16-24MHz range
	UCSCTL6 = XT2DRIVE_2;
	UCSCTL6 &= ~XT2OFF;                     // Enable XT2
	UCSCTL6 |= XT1OFF;						// Disable XT1
	// Loop until XT1,XT2 & DCO stabilizes - in this case loop until XT2 settles
	do
	{
		UCSCTL7 &= ~(XT2OFFG | XT1LFOFFG | XT1HFOFFG | DCOFFG);
												// Clear XT2,XT1,DCO fault flags
		SFRIFG1 &= ~OFIFG;                      // Clear fault flags
	}while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag

	// select XT2 as source for MCLK and SMCLK and ACLK to REFOCLK
	UCSCTL4 = SELA__REFOCLK | SELS__XT2CLK | SELM__XT2CLK;
	// set divider for MCLK to 1 and SMCLK to 8
	// MCLK = 20MHz, SMCLK = 2.5MHz
	UCSCTL5 = DIVM__1 | DIVS__8;
}
