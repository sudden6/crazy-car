/*
 * hal_adc12.h
 *
 *  Created on: 29.11.2016
 *      Author: christoph
 */

#ifndef HAL_HAL_ADC12_H_
#define HAL_HAL_ADC12_H_

#include <inttypes.h>

#define ADC_VREF		2500UL

enum HAL_ADC12_CHANNELS {ADC12_CHAN_RIGHT = 0, ADC12_CHAN_LEFT = 1, ADC12_CHAN_FRONT = 2, ADC12_CHAN_VBAT = 3, ADC12_CHAN_TEMPERATURE = 4, ADC12_CHAN_NUM_CHANNELS = 5};

struct HAL_ADC12 {
	uint8_t volatile Rdy;
	uint16_t volatile Buffer[ADC12_CHAN_NUM_CHANNELS];
	uint16_t volatile FilterBuffer[ADC12_CHAN_NUM_CHANNELS];	// buffer for the lowpass filter
	uint16_t volatile Filtered[ADC12_CHAN_NUM_CHANNELS];		// output of the lowpass filter
};

void HAL_ADC12_Init();
uint16_t HAL_ADC12_GetChannel(enum HAL_ADC12_CHANNELS chan);
uint16_t HAL_ADC12_GetChannelFiltered(enum HAL_ADC12_CHANNELS chan);

#endif /* HAL_HAL_ADC12_H_ */
