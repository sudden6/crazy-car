/*
 * hal_gpio.h
 *
 *  Created on: 11.10.2016
 *      Author: christoph
 */

#ifndef HAL_HAL_GPIO_H_
#define HAL_HAL_GPIO_H_

#include <inttypes.h>
#include <msp430.h>

typedef struct
{
	uint8_t active;	//True 1 / False 0
	uint8_t button; //Button number
}ButtonCom;


volatile ButtonCom buttons;

// macro to concat strings with the preprocessor
#define PPCAT_NX(A, B) A ## B

// macros to get the register names from a corresponding port
#define PORT_REN(PORT)  PPCAT_NX(PORT, REN)
#define PORT_IN(PORT)   PPCAT_NX(PORT, IN)
#define PORT_OUT(PORT)  PPCAT_NX(PORT, OUT)
#define PORT_DIR(PORT)  PPCAT_NX(PORT, DIR)
#define PORT_DS(PORT)   PPCAT_NX(PORT, DS)
#define PORT_SEL(PORT)  PPCAT_NX(PORT, SEL)
#define PORT_SEL2(PORT) PPCAT_NX(PORT, SEL2)
#define PORT_IE(PORT)   PPCAT_NX(PORT, IE)
#define PORT_IES(PORT)  PPCAT_NX(PORT, IES)

// macros to switch a pin to HIGH, LOW or TOGGLE it
#define HIGH(PIN)			(PORT_OUT(PIN ## _PORT) |= PIN)
#define LOW(PIN)			(PORT_OUT(PIN ## _PORT) &= ~PIN)
#define TOGGLE(PIN)			(PORT_OUT(PIN ## _PORT) ^= PIN)

#define RPM_SENSOR				(1 << 3)
#define RPM_SENSOR_PORT			P1

#define RPM_SENSOR_DIR			(1 << 4)
#define RPM_SENSOR_DIR_PORT		P1

#define I2C_INT_MOTION			(1 << 5)
#define I2C_INT_MOTION_PORT		P1

#define START_BUTTON			(1 << 6)
#define START_BUTTON_PORT		P1
#define START_BUTTON_STATE		(!(PORT_IN(START_BUTTON_PORT) & START_BUTTON))

#define STOP_BUTTON				(1 << 7)
#define STOP_BUTTON_PORT 		P1
#define STOP_BUTTON_STATE		(!(PORT_IN(STOP_BUTTON_PORT) & STOP_BUTTON))

#define DEBUG_TXD				(1 << 0)
#define DEBUG_TXD_PORT 			P2

#define DEBUG_RXD				(1 << 1)
#define DEBUG_RXD_PORT			P2

#define AUX_PIN_1				(1 << 2)
#define AUX_PIN_1_PORT			P2

#define AUX_PIN_2				(1 << 3)
#define AUX_PIN_2_PORT			P2

#define AUX_PIN_3				(1 << 4)
#define AUX_PIN_3_PORT			P2

#define AUX_PIN_4				(1 << 5)
#define AUX_PIN_4_PORT			P2

// start
#define I2C_SDA_MOTION			(1 << 6)
#define I2C_SDA_MOTION_PORT		P2

#define I2C_SCL_MOTION			(1 << 7)
#define I2C_SCL_MOTION_PORT		P2

#define THROTTLE				(1 << 2)
#define THROTTLE_PORT			P3

#define STEERING				(1 << 3)
#define STEERING_PORT			P3

#define DISTANCE_FRONT_EN		(1 << 7)
#define DISTANCE_FRONT_EN_PORT	P3

#define LINE_FOLLOW_1			(1 << 4)
#define LINE_FOLLOW_1_PORT		P4

#define LED_BLUE				(1 << 3)
#define LED_BLUE_PORT			P4
#define BLUE_ON                 HIGH(LED_BLUE)
#define BLUE_OFF                LOW(LED_BLUE)

#define LED_GREEN				(1 << 5)
#define LED_GREEN_PORT			P4
#define GREEN_ON                HIGH(LED_GREEN)
#define GREEN_OFF               LOW(LED_GREEN)

#define LED_RED					(1 << 6)
#define LED_RED_PORT			P4
#define RED_ON                 HIGH(LED_RED)
#define RED_OFF                LOW(LED_RED)

#define LINE_FOLLOW_5			(1 << 7)
#define LINE_FOLLOW_5_PORT		P4

#define DISTANCE_RIGHT			(1 << 0)
#define DISTANCE_RIGHT_PORT		P6

#define DISTANCE_LEFT			(1 << 1)
#define DISTANCE_LEFT_PORT		P6

#define DISTANCE_FRONT			(1 << 2)
#define DISTANCE_FRONT_PORT		P6

#define VBAT_MEASURE			(1 << 3)
#define VBAT_MEASURE_PORT		P6

#define DISTANCE_LEFT_EN		(1 << 4)
#define DISTANCE_LEFT_EN_PORT	P6

#define DISTANCE_RIGHT_EN		(1 << 7)
#define DISTANCE_RIGHT_EN_PORT	P9

#define LCD_RESET				(1 << 0)
#define LCD_RESET_PORT			P9

#define LCD_BL					(1 << 0)
#define LCD_BL_PORT				P8
#define LCD_BL_ON				HIGH(LCD_BL)
#define LCD_BL_OFF				LOW(LCD_BL)

#define LCD_SPI_CS				(1 << 1)
#define LCD_SPI_CS_PORT			P8

#define UART_TXD_AUX			(1 << 2)
#define UART_TXD_AUX_PORT		P8

#define UART_RXD_AUX			(1 << 3)
#define UART_RXD_AUX_PORT		P8

#define LCD_SPI_CLK				(1 << 4)
#define LCD_SPI_CLK_PORT		P8

#define LCD_SPI_MOSI			(1 << 5)
#define LCD_SPI_MOSI_PORT		P8

#define LCD_SPI_MISO			(1 << 6)
#define LCD_SPI_MISO_PORT		P8

#define LCD_DATACMD				(1 << 7)
#define LCD_DATACMD_PORT		P8

#define XT2IN					(1 << 2)
#define XT2IN_PORT				P7

#define XT2OUT					(1 << 3)
#define XT2OUT_PORT				P7

#define SMCLK					(1 << 4)
#define SMCLK_PORT				P3

#define US_TRIG                 LINE_FOLLOW_5
#define US_TRIG_PORT            LINE_FOLLOW_5_PORT

#define US_ECHO                 LINE_FOLLOW_1
#define US_ECHO_PORT            LINE_FOLLOW_1_PORT

void HAL_GPIO_Init();

#endif /* HAL_HAL_GPIO_H_ */
