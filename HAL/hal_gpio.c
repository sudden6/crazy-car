/*
 * hal_gpio.c
 *
 *  Created on: 11.10.2016
 *      Author: hansi
 */

#include <inttypes.h>
#include <msp430.h>
#include "hal_gpio.h"
#include "hal_ucs.h"

volatile ButtonCom buttons;

void HAL_GPIO_Init()
{

	/******************************************************************
	* First, before the actual IO-settings set all IO-Port to outputs *
	* with low-power output and state = 0                             *
	******************************************************************/

	P1DIR = 0xff;
	P1SEL = 0x00;
	P1DS = 0x00;
	P1OUT = 0x00;

	P2DIR = 0xff;
	P2SEL = 0x00;
	P2DS = 0x00;
	P2OUT = 0x00;

	P3DIR = 0xff;
	P3SEL = 0x00;
	P3DS = 0x00;
	P3OUT = 0x00;

	P4DIR = 0xff;
	P4SEL = 0x00;
	P4DS = 0x00;
	P4OUT = 0x00;

	P5DIR = 0xff;
	P5SEL = 0x00;
	P5DS = 0x00;
	P5OUT = 0x00;

	P6DIR = 0xff;
	P6SEL = 0x00;
	P6DS = 0x00;
	P6OUT = 0x00;

	P7DIR = 0xff;
	P7SEL = 0x00;
	P7DS = 0x00;
	P7OUT = 0x00;

	P8DIR = 0xff;
	P8SEL = 0x00;
	P8DS = 0x00;
	P8OUT = 0x00;

	P9DIR = 0xff;
	P9SEL = 0x00;
	P9DS = 0x00;
	P9OUT = 0x00;

	/*********************************************************
	* Config the needed IO-Ports/Pins for the actual purpose *
	*********************************************************/

	//RPM_SENSOR
	PORT_DIR(RPM_SENSOR_PORT) &= ~RPM_SENSOR; //use secondary-function (capture-mode of timer a0)
	PORT_SEL(RPM_SENSOR_PORT) |= RPM_SENSOR; //use secondary-function (capture-mode of timer a0)
	//PORT_REN(RPM_SENSOR_PORT) |= RPM_SENSOR; //enable resistor
	//PORT_OUT(RPM_SENSOR_PORT) |= RPM_SENSOR; //enable pullup-resistor


	//RPM_SENSOR_DIR
	PORT_DIR(RPM_SENSOR_DIR_PORT) &= ~RPM_SENSOR_DIR; //input
	PORT_REN(RPM_SENSOR_DIR_PORT) |= RPM_SENSOR_DIR; //enable resistor
	PORT_OUT(RPM_SENSOR_DIR_PORT) |= RPM_SENSOR_DIR; //enable pullup-resistor

	//START_BUTTON
	PORT_DIR(START_BUTTON_PORT) &= ~START_BUTTON; //input
	PORT_REN(START_BUTTON_PORT) |= START_BUTTON; //enable resistor
	PORT_OUT(START_BUTTON_PORT) |= START_BUTTON; //enable pullup-resistor

	//STOP_BUTTON
	PORT_DIR(STOP_BUTTON_PORT) &= ~STOP_BUTTON; //input
	PORT_REN(STOP_BUTTON_PORT) |= STOP_BUTTON; //enable resistor
	PORT_OUT(STOP_BUTTON_PORT) |= STOP_BUTTON; //enable pullup-resistor

	//I2C_INT_MOTION
	PORT_DIR(I2C_INT_MOTION_PORT) |= I2C_INT_MOTION; //output
	PORT_SEL(I2C_INT_MOTION_PORT) &= ~I2C_INT_MOTION; //IO-Function
	PORT_DS(I2C_INT_MOTION_PORT) &= ~I2C_INT_MOTION; //low-power output
	PORT_OUT(I2C_INT_MOTION_PORT) &= ~I2C_INT_MOTION; //set output to low

	//DEBUG_TXD
	PORT_SEL(DEBUG_TXD_PORT) |= DEBUG_TXD; //secondary function
	PORT_DIR(DEBUG_TXD_PORT) |= DEBUG_TXD; //secondary function

	//DEBUG_RXD
	PORT_SEL(DEBUG_RXD_PORT) |= DEBUG_RXD; //secondary function
	PORT_DIR(DEBUG_TXD_PORT) |= DEBUG_TXD; //secondary function

	//AUX_PIN_1 is set to low-power output until we know what to do
	PORT_DIR(AUX_PIN_1_PORT) |= AUX_PIN_1; //output
	PORT_SEL(AUX_PIN_1_PORT) &= ~AUX_PIN_1; //IO-Function
	PORT_DS(AUX_PIN_1_PORT) &= ~AUX_PIN_1; //low-power output
	PORT_OUT(AUX_PIN_1_PORT) &= ~AUX_PIN_1; //set output to low

	//AUX_PIN_2 is set to low-power output until we know what to do
	PORT_DIR(AUX_PIN_2_PORT) |= AUX_PIN_2; //output
	PORT_SEL(AUX_PIN_2_PORT) &= ~AUX_PIN_2; //IO-Function
	PORT_DS(AUX_PIN_2_PORT) &= ~AUX_PIN_2; //low-power output
	PORT_OUT(AUX_PIN_2_PORT) &= ~AUX_PIN_2; //set output to low

	//AUX_PIN_3 is set to low-power output until we know what to do
	PORT_DIR(AUX_PIN_3_PORT) |= AUX_PIN_3; //output
	PORT_SEL(AUX_PIN_3_PORT) &= ~AUX_PIN_3; //IO-Function
	PORT_DS(AUX_PIN_3_PORT) &= ~AUX_PIN_3; //low-power output
	PORT_OUT(AUX_PIN_3_PORT) &= ~AUX_PIN_3; //set output to low

	//AUX_PIN_4 is set to low-power output until we know what to do
	PORT_DIR(AUX_PIN_4_PORT) |= AUX_PIN_4; //output
	PORT_SEL(AUX_PIN_4_PORT) &= ~AUX_PIN_4; //IO-Function
	PORT_DS(AUX_PIN_4_PORT) &= ~AUX_PIN_4; //low-power output
	PORT_OUT(AUX_PIN_4_PORT) &= ~AUX_PIN_4; //set output to low

	//I2C_SDA_MOTION is set to low-power output until we know what to do
	PORT_DIR(I2C_SDA_MOTION_PORT) |= I2C_SDA_MOTION; //output
	PORT_SEL(I2C_SDA_MOTION_PORT) &= ~I2C_SDA_MOTION; //IO-Function
	PORT_DS(I2C_SDA_MOTION_PORT) &= ~I2C_SDA_MOTION; //low-power output
	PORT_OUT(I2C_SDA_MOTION_PORT) |= I2C_SDA_MOTION; //set output to high

	//I2C_SCL_MOTION is set to low-power output until we know what to do
	PORT_DIR(I2C_SCL_MOTION_PORT) |= I2C_SCL_MOTION; //output
	PORT_SEL(I2C_SCL_MOTION_PORT) &= ~I2C_SCL_MOTION; //IO-Function
	PORT_DS(I2C_SCL_MOTION_PORT) &= ~I2C_SCL_MOTION; //low-power output
	PORT_OUT(I2C_SCL_MOTION_PORT) |= I2C_SCL_MOTION; //set output to high

	//THROTTLE is set to CCR1 output, see Table 6-51 in the device specific datasheet
	PORT_DIR(THROTTLE_PORT) |= THROTTLE; //output
	PORT_SEL(THROTTLE_PORT) |= THROTTLE; //Secondary-Function
	PORT_DS(THROTTLE_PORT) &= ~THROTTLE; //low-power output

	//STEERING is set to CCR2 output, see Table 6-51 in the device specific datasheet
	PORT_DIR(STEERING_PORT) |= STEERING; //output
	PORT_SEL(STEERING_PORT) |= STEERING; //Secondary-Function
	PORT_DS(STEERING_PORT) &= ~STEERING; //low-power output

	//DISTANCE_FRONT_EN is set to low-power output until we know what to do
	PORT_DIR(DISTANCE_FRONT_EN_PORT) |= DISTANCE_FRONT_EN; //output
	PORT_SEL(DISTANCE_FRONT_EN_PORT) &= ~DISTANCE_FRONT_EN; //IO-Function
	PORT_DS(DISTANCE_FRONT_EN_PORT) &= ~DISTANCE_FRONT_EN; //low-power output
	PORT_OUT(DISTANCE_FRONT_EN_PORT) &= ~DISTANCE_FRONT_EN; //set output to low

	/* used by ultrasonic sensor
	//LINE_FOLLOW_1
	PORT_DIR(LINE_FOLLOW_1_PORT) &= ~LINE_FOLLOW_1; //input
	PORT_REN(LINE_FOLLOW_1_PORT) |= LINE_FOLLOW_1; //enable resistor
	PORT_OUT(LINE_FOLLOW_1_PORT) |= LINE_FOLLOW_1; //enable pullup-resistor
	*/

	//LED_BLUE
	PORT_DIR(LED_BLUE_PORT) |= LED_BLUE; //output
	PORT_SEL(LED_BLUE_PORT) &= ~LED_BLUE; //IO-Function
	PORT_DS(LED_BLUE_PORT) |= LED_BLUE; //high-power output
	PORT_OUT(LED_BLUE_PORT) &= ~LED_BLUE; //set output to low

	//LED_BLUE
	PORT_DIR(LED_GREEN_PORT) |= LED_GREEN; //output
	PORT_SEL(LED_GREEN_PORT) &= ~LED_GREEN; //IO-Function
	PORT_DS(LED_GREEN_PORT) |= LED_GREEN; //high-power output
	PORT_OUT(LED_GREEN_PORT) &= ~LED_GREEN; //set output to low

	//LED_BLUE
	PORT_DIR(LED_RED_PORT) |= LED_RED; //output
	PORT_SEL(LED_RED_PORT) &= ~LED_RED; //IO-Function
	PORT_DS(LED_RED_PORT) |= LED_RED; //high-power output
	PORT_OUT(LED_RED_PORT) &= ~LED_RED; //set output to low

	/* used by ultrasonic sensor
	//LINE_FOLLOW_5
	PORT_DIR(LINE_FOLLOW_5_PORT) &= ~LINE_FOLLOW_5; //input
	PORT_REN(LINE_FOLLOW_5_PORT) |= LINE_FOLLOW_5; //enable resistor
	PORT_OUT(LINE_FOLLOW_5_PORT) |= LINE_FOLLOW_5; //enable pullup-resistor
	*/

	// ultrasonic sensor echo pin
	PORT_DIR(US_ECHO_PORT) &= ~US_ECHO; //input
	PORT_REN(US_ECHO_PORT) |= US_ECHO; //enable resistor
	PORT_OUT(US_ECHO_PORT) |= US_ECHO; //enable pullup-resistor

	// ultrasonic sensor trigger pin
    PORT_DIR(US_TRIG_PORT) |= US_TRIG; //output
    PORT_SEL(US_TRIG_PORT) &= ~US_TRIG; // IO-function
    PORT_DS(US_TRIG_PORT) |= US_TRIG; //high-power output
    PORT_OUT(US_TRIG_PORT) &= ~US_TRIG; //set output to low

	// DISTANCE_RIGHT is set to ADC-Input, see
	// Table 6-55. Port P6 (P6.0 to P6.7) Pin Functions
	PORT_SEL(DISTANCE_RIGHT_PORT) |= DISTANCE_RIGHT; //secondary function

	// DISTANCE_LEFT is set to ADC-Input, see
	// Table 6-55. Port P6 (P6.0 to P6.7) Pin Functions
	PORT_SEL(DISTANCE_LEFT_PORT) |= DISTANCE_LEFT; //secondary function

	// DISTANCE_FRONT is set to ADC-Input, see
	// Table 6-55. Port P6 (P6.0 to P6.7) Pin Functions
	PORT_SEL(DISTANCE_FRONT_PORT) |= DISTANCE_FRONT; //secondary function

	// VBAT_MEASURE is set to ADC-Input, see
	// Table 6-55. Port P6 (P6.0 to P6.7) Pin Functions
	PORT_SEL(VBAT_MEASURE_PORT) |= VBAT_MEASURE; //secondary function

	//DISTANCE_LEFT_EN is set to low-power output until we know what to do
	PORT_DIR(DISTANCE_LEFT_EN_PORT) |= DISTANCE_LEFT_EN; //output
	PORT_SEL(DISTANCE_LEFT_EN_PORT) &= ~DISTANCE_LEFT_EN; //IO-Function
	PORT_DS(DISTANCE_LEFT_EN_PORT) &= ~DISTANCE_LEFT_EN; //low-power output
	PORT_OUT(DISTANCE_LEFT_EN_PORT) &= ~DISTANCE_LEFT_EN; //set output to low

	//DISTANCE_RIGHT_EN is set to low-power output until we know what to do
	PORT_DIR(DISTANCE_RIGHT_EN_PORT) |= DISTANCE_RIGHT_EN; //output
	PORT_SEL(DISTANCE_RIGHT_EN_PORT) &= ~DISTANCE_RIGHT_EN; //IO-Function
	PORT_DS(DISTANCE_RIGHT_EN_PORT) &= ~DISTANCE_RIGHT_EN; //low-power output
	PORT_OUT(DISTANCE_RIGHT_EN_PORT) &= ~DISTANCE_RIGHT_EN; //set output to low

	//LCD_RESET
	PORT_DIR(LCD_RESET_PORT) |= LCD_RESET; //output
	PORT_SEL(LCD_RESET_PORT) &= ~LCD_RESET; //IO-Function
	PORT_DS(LCD_RESET_PORT) &= ~LCD_RESET; //low-power output
	PORT_OUT(LCD_RESET_PORT) &= ~LCD_RESET; //set output to low

	//LCD_BL is set to low-power output until we know what to do
	PORT_DIR(LCD_BL_PORT) |= LCD_BL; //output
	PORT_SEL(LCD_BL_PORT) &= ~LCD_BL; //IO-Function
	PORT_DS(LCD_BL_PORT) &= ~LCD_BL; //low-power output
	PORT_OUT(LCD_BL_PORT) &= ~LCD_BL; //set output to low

	//LCD_SPI_CS is set to low-power output until we know what to do
	PORT_DIR(LCD_SPI_CS_PORT) |= LCD_SPI_CS; //output
	PORT_SEL(LCD_SPI_CS_PORT) &= ~LCD_SPI_CS; //IO-Function
	PORT_DS(LCD_SPI_CS_PORT) |= LCD_SPI_CS; //high-power output
	PORT_OUT(LCD_SPI_CS_PORT) &= ~LCD_SPI_CS; //set output to low

	//From Table 6-58. Port P8 (P8.0 to P8.7) Pin Functions
	//UART_TXD_AUX is set to secondary function
	PORT_SEL(UART_TXD_AUX_PORT) |= UART_TXD_AUX; //second function

	//From Table 6-58. Port P8 (P8.0 to P8.7) Pin Functions
	//UART_RXD_AUX is set to secondary function
	PORT_SEL(UART_RXD_AUX_PORT) |= UART_RXD_AUX; //second function

	// From device specific datasheet Table 6-58. Port P8 (P8.0 to P8.7) Pin Functions
	// LCD_SPI_CLK is set to secondary function
	PORT_SEL(LCD_SPI_CLK_PORT) |= LCD_SPI_CLK; //secondary function
	PORT_DS(LCD_SPI_CLK_PORT) |= LCD_SPI_CLK; //low-power output

	// From device specific datasheet Table 6-58. Port P8 (P8.0 to P8.7) Pin Functions
	//LCD_SPI_MOSI is set to secondary function
	PORT_SEL(LCD_SPI_MOSI_PORT) |= LCD_SPI_MOSI; //secondary function
	PORT_DS(LCD_SPI_MOSI_PORT) |= LCD_SPI_MOSI; //high-power output

	// From device specific datasheet Table 6-58. Port P8 (P8.0 to P8.7) Pin Functions
	//LCD_SPI_MISO is set to secondary function
	PORT_SEL(LCD_SPI_MISO_PORT) |= LCD_SPI_MISO; //IO-Function
	PORT_DS(LCD_SPI_MISO_PORT) &= ~LCD_SPI_MISO; //low-power output

	//LCD_DATACMD is set to low-power output until we know what to do
	PORT_DIR(LCD_DATACMD_PORT) |= LCD_DATACMD; //output
	PORT_SEL(LCD_DATACMD_PORT) &= ~LCD_DATACMD; //IO-Function
	PORT_DS(LCD_DATACMD_PORT) |= LCD_DATACMD; //high-power output
	PORT_OUT(LCD_DATACMD_PORT) &= ~LCD_DATACMD; //set output to low

	// From device specific datasheet Table 6-56. Port P7 (P7.2 and P7.3) Pin Functions
	//XT2IN is used for the Crystal Oszilator

	PORT_SEL(XT2IN_PORT) |= XT2IN; // secondary function
	//XT2OUT is used for the Crystal Oszilator
	PORT_SEL(XT2OUT_PORT) |= XT2OUT; // secondary function

	// SMCLK pin to output the SMCLK
	PORT_DIR(SMCLK_PORT) |= SMCLK; // output
	PORT_SEL(SMCLK_PORT) |= SMCLK; // secondary function

}

#pragma vector=PORT1_VECTOR
__interrupt void Port_1_ISR(void)
{
	/*
	if(P1IFG & START_BUTTON)
	{
	   buttons.active = 1;
	   buttons.button = START_BUTTON;
	   P1IFG &= ~START_BUTTON;
	}
	if(P1IFG & STOP_BUTTON)
	{
	   buttons.active = 1;
	   buttons.button = STOP_BUTTON;
	   P1IFG &= ~STOP_BUTTON;
	}
	//*/


}



