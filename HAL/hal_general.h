/*
 * hal_general.h
 *
 *  Created on: 11.10.2016
 *      Author: christoph
 */

#ifndef HAL_HAL_GENERAL_H_
#define HAL_HAL_GENERAL_H_

#include "hal_usciB1.h"

void HAL_Init();

#endif /* HAL_HAL_GENERAL_H_ */
