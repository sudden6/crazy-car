/*
 * hal_ucs.h
 *
 *  Created on: 25.10.2016
 *      Author: christoph
 */

#ifndef HAL_HAL_UCS_H_
#define HAL_HAL_UCS_H_

#define MCLK_FREQU 20000000
#define SMCLK_FREQU 2500000

void HAL_UCS_Init();

#endif /* HAL_HAL_UCS_H_ */
