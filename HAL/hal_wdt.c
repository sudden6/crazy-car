/*
 * hal_wdt.c
 *
 *  Created on: 11.10.2016
 *      Author: christoph
 */

#include <msp430.h>

void HAL_Wdt_Init()
{
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
}
