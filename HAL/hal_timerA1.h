/*
 * hal_timerB0.h
 *
 *  Created on: 25.10.2016
 *      Author: hansi
 */

#ifndef HAL_HAL_TIMERA1_H_
#define HAL_HAL_TIMERA1_H_

#include <inttypes.h>

#define CHANNEL_STEERING 2
#define CHANNEL_THROTTLE 1

void HAL_TimerA1_Init();
void HAL_TimerA1_SetPulseLength(uint16_t length_us, uint16_t channel);


#endif /* HAL_HAL_TIMERB0_H_ */
