#include <msp430.h>
#include <inttypes.h>
#include "hal_gpio.h"
#include "hal_ucs.h"
#include "hal_timerA0.h"
#include "../util/controller.h"

volatile CaptureVal capture_val_2;

static uint8_t volatile *update_pid = 0;

void HAL_TimerA0_Init()
{
	//configure timer for capture-mode
	//capture-mode for pin RPM_SENSOR

	TA0CCTL2 |= CAP;			//enable capture-mode
	TA0CCTL2 |= CM_3;			//enable capture-mode on both edges
	TA0CCTL2 |= SCS;			//synchronize the capture source to the timer-clock
	TA0CCTL2 |= CCIS_0;			//use capture-input from pin
	TA0CCTL2 |= CCIE;			//enable capture-interrupt

	TA0CCTL3 |= CCIE;			//Timer-Interrupt for PID-Controllers
	//TA0CCTL4 |= CCIE;			//Timer-Interrupt for PID-Controllers

	TA0CTL |= TASSEL__SMCLK;	//select SM-Clock
	TA0CTL |= MC_2;				//continuous mode
	TA0CTL |= TAIE;				//enable interrupt
	TA0CTL |= TACLR;			//start timer

	capture_val_2.covered_distance = 0;	//set measured distance 0
}

#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer_A0_ISR (void)
{
#define CCR_INCR_PID	(SMCLK_FREQU / PID_UPDATE_FREQ)
	static uint8_t overflows = 0;
	static uint16_t last_tick = 0;

	static int32_t  last_distance = 0;

	uint16_t current_tick;

	if(TA0CCTL2 & CCIFG) //capture-interrupt -> distance measurement
	{
		TA0CCTL2 &= ~CCIE; //disable interrupt
		current_tick = TA0CCR2;
		if(overflows > 10)
		{
//			capture_val_2.speed = 0;
		}
		else
		{
			capture_val_2.speed = ((uint32_t)current_tick + ((uint32_t)overflows)*0xFFFF) - last_tick;
		}
		overflows = 0;
		last_tick = current_tick;

		if(PORT_IN(RPM_SENSOR_DIR_PORT) & RPM_SENSOR_DIR)
		{
			//reverse direction
			capture_val_2.covered_distance--;
			capture_val_2.speed = -capture_val_2.speed;
		}
		else
		{
			//forward direction
			capture_val_2.covered_distance++;
		}


		TA0CCTL2 |= CCIE; //enable innterupt


		TA0IV &= ~TA0IV_TACCR2;	//delete interrupt-flag
	}

	if(TA0CCTL3 & CCIFG)
	{
		TA0CCR3 += CCR_INCR_PID;
		*update_pid = 1;
		TA0IV &= ~TA0IV_TACCR3; //delete interrupt flag
	}

	//*
	if(TA0CTL & TAIFG) //overflow intterupt -> speed measurement
	{
		overflows++;
		if(overflows > 11)
		{
			overflows = 11;
		}
	}
	//*/
}

void HAL_TimerA0_HandlePIDController(uint8_t volatile *update)
{
	update_pid = update;
}
